

# INF 19.02

ru.kpfu.itis.group905.<фамилия>.inf.exceptions 

Exceptions

- 1 - 1/1
- 2 - 1/1
- 3 - 1/1
- 4 - 1/1
- 5 - 1/1
- 6 - 1/1

# ALG 21.02 

ru.kpfu.itis.group905.<фамилия>.algorithms.pair1

LinkedList + Sorting + Accountable

- LinkedList - 9/9
- merge - 2/2
- diff - 1/2

Работает не так, как предполагалось
- intersection - 2/2
- generic Accountable - 5/5

# INF 21.02 

ru.kpfu.itis.group905.<фамилия>.inf.exceptions

Exceptions 2

- IDNumber - 5/5
- Point - 5/5
- Convert - 5/5
- SumException - 5/5
- RunnerProcessor - 5/5

В целом да, но оригинальное сообщение вы потеряли, и это ни оч)
- ThrowsExceptions - 5/5

# ALG 28.02 

ru.kpfu.itis.group905.<фамилия>.algorithms.pair2

Sorting

- merge - 2/2
- largest number - 2/2

# INF 29.02

ru.kpfu.itis.group905.<фамилия>.inf.generics

Generics

- LinkedList - 5/5
- Array - 5/5

Присваивать массивы через простое = не лучшая идея
- test on Account<String, Integer> - 5/5

# INF 07.03 

ru.kpfu.itis.group905.<фамилия>.inf.comparable

Comparable

- 1 - 5/5
- 2 - 5/5
- 3 - 5/5
- 4 - 5/5


# INF 10.03 

ru.kpfu.itis.group905.<фамилия>.inf.maps

Maps

- 1 - 5/5
- 2 - 5/5

# ALG 13.03 

ru.kpfu.itis.group905.<фамилия>.algorithms.pair3

Sets

- 1 - 2/2
- 2 - 2/2

# INF KR1 11.03 10:10

ru.kpfu.itis.group905.<фамилия>.inf.kr1

10/10


# INF 24.03

ru.kpfu.itis.group905.<фамилия>.inf.iterator

Iterator

- 1 - 5/5
- 2 - 5/5
- 3 - 5/5

# ALG 27.03

ru.kpfu.itis.group905.<фамилия>.algorithms.pair4

- 1 - 2/2
- 2 - 2/2
- 3 - 2/2

# INF 30.03

ru.kpfu.itis.group905.<фамилия>.inf.tree

Trees

- 1 - 4/5

justification???
- 2 - 5/5
- 3 - 5/5
- 4 - 5/5

# ALG 03.04

ru.kpfu.itis.group905.<фамилия>.algorithms.pair5

Dynamics

- 1 - 2/2
- 2 - 2/2
- 3 - 2/2


# ALG SEM1 03.04



# INF Stack

ru.kpfu.itis.group905.<фамилия>.inf.stack

* это даты пар, на которых должно было выполняться задание
- Stack (14.04) - 10/10
- RPN (15.04) - 10/10
- RPN reformat (21.04) - 10/10
- combine all (22.04) - 10/10
- sorting (22.04) - 10/10


# ALG 24.04

ru.kpfu.itis.group905.<фамилия>.algorithms.pair7

- 1 - 2/2
- 2 - 2/2

Жду исправлений по примеру с пары




# ALG 01.05

ru.kpfu.itis.group905.<фамилия>.algorithms.pair8

- 1 - 2
- 2 - 2
- 3 - 2
- 4 - 2


# INF 06.05

ru.kpfu.itis.group905.<фамилия>.inf.stack

- reformat - 7


# INF 08.05

ru.kpfu.itis.group905.<фамилия>.inf.strings

- 1 - 5
- 2 - 5
- 3 - 5

Не усложняйте, вам запретили тольок один единственный метод, это не значит что нужно писать с нуля
- 4 - 5

Тут вообще совсем сложно: нужно поделить по словам и вывести слова через одного

в три строчки делается (ну хорошо, в 5 если считать {} за отдельные строки)


# INF 11.05

ru.kpfu.itis.group905.<фамилия>.inf.regex

- 1 - 5
- 2 - 5
- 3 - 5


# ALG 15.05

ru.kpfu.itis.group905.<фамилия>.algorithms.pair9

- 1 - 2
- 2 - 2
- 3 - 2
- 4 - 2


# INF 18.05

ru.kpfu.itis.group905.<фамилия>.inf.files

- java files - 5
- url - 5


# ALG 20.05

ru.kpfu.itis.group905.<фамилия>.algorithms.pair10

- 1 - 2
- 2 - 2
- 3 - 1
- 4 - 2


# INF 24.05

ru.kpfu.itis.group905.<фамилия>.inf.streamApi

- 1 - 5
- 2 - 5





# INF 26.05 12:00



- Fuel - [x] 
- Oxidizer - [x]
- Kerosene и Hydrogen - [x]
- LiquidOxygen - [x]
- InsufficientMatterException - [x]
- Tank<T> - [x]
- UnstableEngineException - [x]
- RocketComparator - [x]
- Code style - [x]
- It works - [x]

Total: 10

