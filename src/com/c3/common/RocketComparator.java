package com.c3.common;

import com.c3.exceptions.UnstableEngineException;

import java.util.Comparator;

public class RocketComparator implements Comparator<Rocket> {

    @Override
    public int compare(Rocket rocket1, Rocket rocket2) {
        return getRocketPower(rocket1).compareTo(getRocketPower(rocket2));
    }

    private Integer getRocketPower(Rocket rocket) {
        int power = 0;

        try {
            while (true) {
                power += rocket.burn();
            }
        } catch (UnstableEngineException ignored) { }

        return power;
    }
}
