package com.c3.common;

import com.c3.exceptions.InsufficientMatterException;

/**
 * Реализация интерфейса IMatter по умолчанию
 */
abstract public class Matter implements IMatter {
    private int mass;
    public Matter(int initialMass) {
        mass = initialMass;
    }
    public int getMass() {
        return mass;
    }
    public void decreaseMass(int mass) throws InsufficientMatterException {
        if (this.mass < mass)
            throw new InsufficientMatterException(this.mass);
        this.mass -= mass;
    }
}
