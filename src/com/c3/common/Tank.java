package com.c3.common;

import com.c3.exceptions.InsufficientMatterException;

public class Tank<T extends IMatter> {
    private T initialFuel;

    public Tank(T initialFuel) {
        this.initialFuel = initialFuel;
    }

    public T next(int mass) throws InsufficientMatterException {
        initialFuel.decreaseMass(mass);
        return initialFuel;
    }

    public int remainingMass() {
        return initialFuel.getMass();
    }
}
