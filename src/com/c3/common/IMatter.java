package com.c3.common;

import com.c3.exceptions.InsufficientMatterException;

/**
 * Некое вещество
 * getMass() возвращает (оставшуюся) массу вещества
 * decreaseMass() уменьшает массу вещества на заданную массу mass
 */
public interface IMatter {
    int getMass();
    void decreaseMass(int mass) throws InsufficientMatterException;
}
