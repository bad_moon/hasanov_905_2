package com.c3.test;

import com.c3.common.Rocket;
import com.c3.exceptions.UnstableEngineException;

/**
 * Created by red on 06.05.14.
 */
public class TestRocket extends Rocket {
    private int count;
    private int power;

    public TestRocket(int count, int power) {
        super();
        this.count = count;
        this.power = power;
    }

    @Override
    public int burn() throws UnstableEngineException {
        if (count <= 0)
            throw new UnstableEngineException();
        count--;
        return power;
    }
}
