package com.c3.fuels;

import com.c3.common.Matter;

/**
 * Kerosene
 * Формула: C12H26
 */

public class Kerosene extends Matter implements Fuel {
    private int mass;

    public Kerosene(int mass){
        super(mass);
    }

    @Override
    public int getCarbonAtomsCount() {
        return 12;
    }

    @Override
    public int getHydrogenAtomsCount() {
        return 26;
    }

    @Override
    public int getNitrogenAtomsCount() {
        return 0;
    }

    @Override
    public int getOxygenAtomsCount() {
        return 0;
    }
}