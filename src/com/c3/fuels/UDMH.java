package com.c3.fuels;

import com.c3.common.Matter;

/**
 * UDMH = Unsymmetrical DiMethylHydrazine
 * Несимметричный диметилгидразин (НДМГ, не путать с НГМД), он же 1,1-диметилгидразин, он же "гептил"
 * Формула: H2N-N(CH3)2
 */
public class UDMH extends Matter implements Fuel {
    private int mass;

    public UDMH(int mass){
        super(mass);
    }

    @Override
    public int getCarbonAtomsCount() {
        return 2;
    }

    @Override
    public int getHydrogenAtomsCount() {
        return 8;
    }

    @Override
    public int getNitrogenAtomsCount() {
        return 2;
    }

    @Override
    public int getOxygenAtomsCount() {
        return 0;
    }
}
