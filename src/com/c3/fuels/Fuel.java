package com.c3.fuels;

import com.c3.common.IMatter;

public interface Fuel extends IMatter {

    int getOxygenAtomsCount();

    int getNitrogenAtomsCount();

    int getHydrogenAtomsCount();

    int getCarbonAtomsCount();
}
