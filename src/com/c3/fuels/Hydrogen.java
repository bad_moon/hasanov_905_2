package com.c3.fuels;

import com.c3.common.Matter;

/**
 * Hydrogen
 * Формула: H2
 */

public class Hydrogen extends Matter implements Fuel {
    private int mass;

    public Hydrogen(int mass){
        super(mass);
    }

    @Override
    public int getCarbonAtomsCount() {
        return 0;
    }

    @Override
    public int getHydrogenAtomsCount() {
        return 2;
    }

    @Override
    public int getNitrogenAtomsCount() {
        return 0;
    }

    @Override
    public int getOxygenAtomsCount() {
        return 0;
    }
}
