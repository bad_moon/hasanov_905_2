package com.c3.oxidizers;

import com.c3.common.Matter;

/**
 * Liquid oxygen
 * Формула: O2
 */

public class LiquidOxygen extends Matter implements Oxidizer {

    public LiquidOxygen(int initialMass) {
        super(initialMass);
    }

    @Override
    public int getNitrogenAtomsCount() {
        return 0;
    }

    @Override
    public int getOxygenAtomsCount() {
        return 2;
    }
}
