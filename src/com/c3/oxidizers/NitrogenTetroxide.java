package com.c3.oxidizers;

import com.c3.common.Matter;

/**
 * Nitrogen Tetroxide = тетраоксид азота
 * Формула: N2O4
 */
public class NitrogenTetroxide extends Matter implements Oxidizer {

    public NitrogenTetroxide(int initialMass) {
        super(initialMass);
    }

    @Override
    public int getNitrogenAtomsCount() {
        return 2;
    }

    @Override
    public int getOxygenAtomsCount() {
        return 4;
    }
}
