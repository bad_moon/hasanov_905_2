package com.c3.oxidizers;

import com.c3.common.IMatter;

public interface Oxidizer extends IMatter {

    int getOxygenAtomsCount();

    int getNitrogenAtomsCount();
}
