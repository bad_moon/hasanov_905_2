package com.c3.exceptions;

public class InsufficientMatterException extends RocketException {
    private int left;

    public InsufficientMatterException(int left) {
        super("Insufficient matter, left: " + left);
        this.left = left;
    }

    public int getLeft() {
        return left;
    }
}
