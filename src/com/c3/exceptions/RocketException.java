package com.c3.exceptions;

public class RocketException extends Exception {

    public RocketException(String message) {
        super(message);
    }
}
