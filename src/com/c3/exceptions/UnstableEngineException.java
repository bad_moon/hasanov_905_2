package com.c3.exceptions;

public class UnstableEngineException extends RocketException {
    private int oxidizerLeft;
    private int fuelLeft;

    public UnstableEngineException() {
        super("");
        oxidizerLeft = 0;
        fuelLeft = 0;
    }

    public UnstableEngineException(String message) {
        super(message);
        oxidizerLeft = 0;
        fuelLeft = 0;
    }

    public UnstableEngineException(int fuelLeft, int oxidizerLeft) {
        super("Unstable engine, fuel left: " + fuelLeft
                + " and oxidizer left: " + oxidizerLeft);
        this.oxidizerLeft = 0;
        this.oxidizerLeft = 0;
    }

    public int getOxidizerLeft() {
        return oxidizerLeft;
    }

    public int getFuelLeft() {
        return fuelLeft;
    }
}
