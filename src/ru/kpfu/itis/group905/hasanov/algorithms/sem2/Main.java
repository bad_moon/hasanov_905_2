package ru.kpfu.itis.group905.hasanov.algorithms.sem2;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.PrintWriter;
import java.util.Scanner;

class Main {
    public static void main(String[] args) throws FileNotFoundException {
        String path = "src\\ru\\kpfu\\itis\\group905\\hasanov\\algorithms\\sem2\\";
        Scanner scanner;
        PrintWriter printWriter = new PrintWriter(path + "out.txt");

        long a, b;
        for (int j = 1; j < 101; j++) {
            scanner = new Scanner(new FileReader(path + "input\\in" + j + ".txt"));
            int[] array = new int[scanner.nextInt()];

            for (int i = 0; i < array.length; i++) {
                array[i] = scanner.nextInt();
            }

            a = System.nanoTime();
            Smoothsort.smoothsort(array);
            b = System.nanoTime();
            printWriter.println((b - a) / 100);
            //printWriter.println(Smoothsort.getIteration());
        }
        printWriter.close();
    }
}