package ru.kpfu.itis.group905.hasanov.algorithms.sem2;

import java.io.FileNotFoundException;
import java.io.PrintWriter;

public class RandomIntNumbers {
    public static void main(String[] args) throws FileNotFoundException {
        for (int i = 1; i < 101; i++) {
            make(i * 100, i + "");
        }
    }

    public static void make(int count, String fileName) throws FileNotFoundException {
        String path = "src\\ru\\kpfu\\itis\\group905\\hasanov\\algorithms\\sem2\\input\\";
        PrintWriter printWriter = new PrintWriter(path + "in" + fileName + ".txt");

        printWriter.print(count + "\n");
        for (int i = 0; i < count; i++) {
            printWriter.print((int) (Math.random() * Integer.MAX_VALUE) + "\n");
        }

        printWriter.close();
    }

}
