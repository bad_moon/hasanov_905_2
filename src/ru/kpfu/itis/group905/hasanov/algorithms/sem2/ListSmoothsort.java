package ru.kpfu.itis.group905.hasanov.algorithms.sem2;

import java.util.LinkedList;

public class ListSmoothsort {
    private static int iteration;

    private static final int[] L = { 1, 1, 3, 5, 9, 15, 25, 41, 67, 109, 177, 287, 465, 753,
            1219, 1973, 3193, 5167, 8361, 13529, 21891, 35421, 57313, 92735, 150049,
            242785, 392835, 635621, 1028457, 1664079, 2692537, 4356617, 7049155,
            11405773, 18454929, 29860703, 48315633, 78176337, 126491971, 204668309,
            331160281, 535828591, 866988873
    };

    public static void smoothsort(LinkedList<Integer> list) {
        iteration = 0;
        int N = list.size();
        int[] orders = new int[(int)(Math.log10(N) / Math.log10(2)) * 2];
        int trees = 0;

        for (int i = 0; i < N; i++) {
            if (trees > 1 && orders[trees-2] == orders[trees-1] + 1) {
                trees--;
                orders[trees-1]++;
            }
            else if (trees > 0 && orders[trees-1] == 1) {
                orders[trees++] = 0;
            }
            else {
                orders[trees++] = 1;
            }

            findAndSift(list, i, trees-1, orders);

        }

        for (int i = N-1; i > 0; i--) {
            if (orders[trees-1] <= 1) {
                trees--;
            }
            else {
                int ri = i-1;
                int li = ri - L[orders[trees-1] - 2];

                trees++;
                orders[trees-2]--;
                orders[trees-1] = orders[trees-2] - 1;

                findAndSift(list, li, trees - 2, orders);
                findAndSift(list, ri, trees - 1, orders);
            }
        }

    }

    private static void findAndSift(LinkedList<Integer> list, int i, int tree, int[] orders) {
        int v = list.get(i);
        while (tree > 0) {
            int pi = i - L[orders[tree]];
            if (list.get(pi) <= v) {
                break;
            }
            else if (orders[tree] > 1) {
                int ri = i-1;
                int li = ri - L[orders[tree] - 2];
                if (list.get(pi) <= list.get(li) || list.get(pi) <= list.get(ri)) {
                    break;
                }
            }

            list.set(i, list.get(pi));
            i = pi;
            tree--;
        }
        list.set(i, v);
        siftDown(list, i, orders[tree]);
    }

    private static void siftDown(LinkedList<Integer> list, int i, int order) {
        int v = list.get(i);
        while (order > 1) {
            int ri = i - 1;
            int li = ri - L[order - 2];
            if (v >= list.get(li) && v >= list.get(ri)) {
                break;
            }
            else if (list.get(li) <= list.get(ri)) {
                list.set(i, list.get(ri));
                i = ri;
                order -= 2;
            }
            else {
                list.set(i, list.get(li));
                i = li;
                order -= 1;
            }
            iteration++;
        }
        list.set(i, v);
    }

    public static int getIteration() {
        return iteration;
    }
}