package ru.kpfu.itis.group905.hasanov.algorithms.pair8;

public class Task3 {
    public static void main(String[] args) {
        int[] array = new int[] {5, 7, 7, 8, 8, 10};
        System.out.println(searchEntries(array, 8));
    }

    public static String searchEntries(int[] array, int key) {
        int left = -1;
        int right = array.length;
        int pointer, initialIndex, finalIndex;
        while (left < right - 1)     {
            pointer = (left + right) / 2;
            if (array[pointer] < key) {
                left = pointer;
            } else {
                right = pointer;
            }
        }
        if (right != array.length && array[right] == key) {
            initialIndex = right;
            left = right - 1;
            right = array.length;
            while (left < right - 1)     {
                pointer = (left + right) / 2;
                if (array[pointer] == key) {
                    left = pointer;
                } else {
                    right = pointer;
                }
            }
            finalIndex = right - 1;
        } else {
            initialIndex = -1;
            finalIndex = -1;
        }
        return "[" + initialIndex + ", " + finalIndex + "]";
    }
}
