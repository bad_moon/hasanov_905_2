package ru.kpfu.itis.group905.hasanov.algorithms.pair8;

// Делал под задание, конечно, можно сделать и проще
public class Task1 {
    public static void main(String[] args) {
        System.out.println(binSqrt(150));
    }

    public static int binSqrt(int num) {
        int left = -1;
        int right = num;
        int pointer;
        while (left < right - 1) {
            pointer = (left + right) / 2;
            if (pointer * pointer < num) {
                left = pointer;
            } else {
                right = pointer;
            }
        }
        if (right * right != num) {
            right -= 1;
        }
        return right;
    }
}
