package ru.kpfu.itis.group905.hasanov.algorithms.pair8;

public class Task2 {
    public static void main(String[] args) {
        int[] array = new int[] {8, 7, 6, 5, 4, 3, 2, 1, 3, 4, 5, 6};
        System.out.println(searchDelimiter(array));
    }

    public static int searchDelimiter(int[] array) {
        int left = -1;
        int right = array.length;
        int pointer;
        while (left < right - 1) {
            pointer = (left + right) / 2;
            if (pointer > 0 && array[pointer] < array[pointer - 1]) {
                left = pointer;
            } else {
                right = pointer;
            }
        }
        return right - 1;
    }
}
