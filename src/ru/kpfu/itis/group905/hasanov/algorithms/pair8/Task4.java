package ru.kpfu.itis.group905.hasanov.algorithms.pair8;

public class Task4 {
    public static void main(String[] args) {
        System.out.println("index = " + search(new int[] {6, 7, 8, 9, 10, 1, 2, 3, 4, 5}, 4));
    }

    public static int search(int[] array, int key) {
        int left = 0;
        int newArrayIndex = array.length;
        int point;
        while (left < newArrayIndex - 1) {
            point = (left + newArrayIndex) / 2;
            if (array[left] < array[point]) {
                left = point;
            } else {
                newArrayIndex = point;
            }
        }
        int result = binSearch(array, 0, newArrayIndex - 1, key);
        if (result == -1) {
            result = binSearch(array, newArrayIndex, array.length - 1, key);
        }
        return result;
    }

    public static int binSearch(int[] array, int initialIndex,
                                int finalIndex,  int key) {
        int left = initialIndex - 1;
        int right = finalIndex + 1;
        int pointer;
        while (left < right - 1)     {
            pointer = (left + right) / 2;
            if (array[pointer] < key) {
                left = pointer;
            } else {
                right = pointer;
            }
        }
        if (right == array.length || array[right] != key) {
            return -1;
        }
        return right;
    }
}
