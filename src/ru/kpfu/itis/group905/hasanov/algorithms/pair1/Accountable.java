/*
 * @author Anvar Hasanov
 * 11-905
 * Task #3
 */

package ru.kpfu.itis.group905.hasanov.algorithms.pair1;

interface Accountable<T> {
    T getId();
    T getSum();
    void setSum(int sum);
}