/*
* @author Anvar Hasanov
* 11-905
* Task #2
 */

package ru.kpfu.itis.group905.hasanov.algorithms.pair1;

import java.util.Arrays;

public class SortingAlgorithms {
    public static void main(String[] args) {
        int[] arr = new int[] {0, 1, 1, 2, 3, 4, 5, 32};
        int[] arr2 = new int[] {1, 2, 4, 4, 4, 5, 9, 10, 15};

        System.out.println(Arrays.toString(merge(arr, arr2)));
        System.out.println(Arrays.toString(diff(arr2, arr)));
        System.out.println(Arrays.toString(intersection(arr2, arr)));
    }

    public static int[] merge(int[] firstArr, int[] secondArr) {
        int[] result = new int[firstArr.length + secondArr.length];
        int count = 0;
        int i = 0;
        while (i < firstArr.length) {
            while(count - i < secondArr.length
                    && firstArr[i] >= secondArr[count - i]) {
                result[count] = secondArr[count - i];
                count++;
            }
            result[count] = firstArr[i];
            count++;
            i++;
        }
        while (secondArr.length > count - i) {
            result[count] = secondArr[count - i];
            count++;
        }
        return result;
    }

    public static int[] diff(int[] firstArr, int[] secondArr) {
        int[] temp = new int[firstArr.length + secondArr.length];
        int count = 0;
        if (firstArr.length > secondArr.length) {
            int[] tempArr = firstArr;
            firstArr = secondArr;
            secondArr = tempArr;
        }
        int j = 0;
        int i = 0;
        while (i < firstArr.length && j < secondArr.length) {
            while (j < secondArr.length && firstArr[i] > secondArr[j]) {
                temp[count] = secondArr[j];
                count++;
                j++;
            }
            if (j < secondArr.length) {
                if (firstArr[i] < secondArr[j]) {
                    temp[count] = firstArr[i];
                    count++;
                }
                if (firstArr[i] == secondArr[j]) {
                    j++;
                }
            }
            i++;
        }
        while (j < secondArr.length) {
            temp[count] = secondArr[j];
            count++;
            j++;
        }
        int[] result = new int[count];
        System.arraycopy(temp, 0, result, 0, count);
        return result;
    }

    public static int[] intersection(int[] firstArr, int[] secondArr) {
        int i = 0, j = 0;
        int[] tempArr = new int[firstArr.length];
        int count = 0;
        while (i < firstArr.length && j < secondArr.length) {
            if (firstArr[i] == secondArr[j]) {
                tempArr[count] = firstArr[i];
                count++;
                while (i + 1 < firstArr.length
                        && firstArr[i + 1] == tempArr[count - 1]) {
                    i++;
                }
                while (j < secondArr.length
                        && secondArr[j] == tempArr[count - 1]) {
                    j++;
                }
            }
            else if (firstArr[i] > secondArr[j]) {
                j++;
            }
            else {
                i++;
            }
        }
        int[] result = new int[count];
        System.arraycopy(tempArr, 0, result, 0, count);
        return result;
    }
}


