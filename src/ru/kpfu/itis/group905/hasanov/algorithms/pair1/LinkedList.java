/*
* @author Anvar Hasanov
* 11-905
* Task #1
 */

package ru.kpfu.itis.group905.hasanov.algorithms.pair1;

import java.util.*;

public class LinkedList {
    private Node head;
    private Node tail;
    private int size;

    LinkedList() {
        Scanner sc = new Scanner(System.in);
        try {
            System.out.println("Enter no number to stop");
            while (true) {
                add(sc.nextInt());
            }
        } catch (InputMismatchException | NullPointerException ignored) {
        }
    }

    public void add(int number) {
        Node newNode = new Node();
        newNode.elem = number;
        if (this.head == null) {
           this.head = newNode;
        }
        else {
            this.tail.next = newNode;
            newNode.previous = this.tail;
        }
        this.tail = newNode;
        size++;
    }

    public int max() {
        if (this.head != null) {
            int result = head.elem;
            Node node = head.next;

            while(node != null) {
                if (result < node.elem) {
                    result = node.elem;
                }
                node = node.next;
            }
            return result;
        }
        else {
            return 0;
        }
    }

    public int sum() {
        if (this.head != null) {
            int result = head.elem;
            Node node = head.next;

            while(node != null) {
                result += node.elem;
                node = node.next;
            }
            return result;
        }
        else {
            return 0;
        }
    }

    public boolean hasNegative() {
        Node node = this.head;
        while (node != null) {
            if (node.elem < 0) {
                return true;
            }
            node = node.next;
        }
        return false;
    }

    public void removeHead() {
        if (this.head != null) {
            if (this.head.next != null) {
                this.head = this.head.next;
                this.head.previous = null;
            }
            else {
                this.head = null;
                this.tail = null;
            }
            size--;
        }
    }

    public void removeTail() {
        if (this.head != null) {
            if (this.tail.previous != null) {
                this.tail = this.tail.previous;
                this.tail.next = null;
            }
            else {
                this.head = null;
                this.tail = null;
            }
            size--;
        }
    }

    public void remove(int index) {
        if (index >= 0 && index < size) {
            Node temp = head;
            while (index != 0) {
                temp = temp.next;
                index--;
            }
            removeElement(temp);
        }
    }

    public void removeAll(int number) {
        Node temp = this.head;
        int sizeHolder = size;
        for (int i = 0; i < sizeHolder; i++) {
            if (temp.elem == number) {
                removeElement(temp);
            }
            temp = temp.next;
        }
    }

    private void removeElement(Node element) {
        if (element.equals(this.head)) {
            removeHead();
        }
        else if (element.equals(this.tail)) {
            removeTail();
        }
        else {
            element.previous.next = element.next;
            element.next.previous = element.previous;
            size--;
        }
    }

    public void insert(int number, int element) {
        Node temp = head;
        boolean flag = false;
        for (int i = 0; i < size; i++) {
            if (temp.elem == element) {
                flag = true;
                break;
            }
            temp = temp.next;
        }
        if (flag) {
            pasteInFrontOfNodeNewElement(temp, number);

            pasteAfterOfNodeNewElement(temp, number);
        }
    }

    private void pasteInFrontOfNodeNewElement(Node node, int element) {
        Node newNode = new Node();
        newNode.elem = element;
        newNode.next = node;
        if (node.equals(this.head)) {
            this.head = newNode;
        }
        else {
            node.previous.next = newNode;
        }
        newNode.previous = node.previous;
        node.previous = newNode;
        size++;
    }

    private void pasteAfterOfNodeNewElement(Node node, int element) {
        Node newNode = new Node();
        newNode.elem = element;
        newNode.previous = node;
        if (node.equals(this.tail)) {
            this.tail = newNode;
        }
        else {
            node.next.previous = newNode;
        }
        newNode.next = node.next;
        node.next = newNode;
        size++;
    }

    @Override
    public String toString() {
        String result = "";
        Node node = head;

        for (int i = 0; i < size; i++) {
            result += node.elem + " ";
            node = node.next;
        }
        return result;
    }

    public int getSize() {
        return this.size;
    }

}
