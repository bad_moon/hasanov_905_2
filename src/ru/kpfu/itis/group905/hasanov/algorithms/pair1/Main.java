package ru.kpfu.itis.group905.hasanov.algorithms.pair1;

public class Main {
    public static void main(String[] args) {

       LinkedList my = new LinkedList();

        my.removeHead();
        my.removeTail();
        my.remove(2);
        my.removeAll(7);
        System.out.println(my.hasNegative());
        System.out.println(my.max());
        System.out.println(my.sum());
        my.insert(2,8 );
        System.out.println("Size - " + my.getSize());
        System.out.println(my);

        Account<Double> acc = new Account<>(1234.1, 100.4);
        acc.addToSum(150.2);
        acc.subtractFromSum(25.2);
        System.out.println(acc.getSum());
        acc.setSum(666);
        System.out.println(acc.getSum());
        System.out.println(acc.getId());
    }
}
