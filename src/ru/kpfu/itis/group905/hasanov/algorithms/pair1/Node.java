/*
 * @author Anvar Hasanov
 * 11-905
 * Task #1
 */

package ru.kpfu.itis.group905.hasanov.algorithms.pair1;

public class Node {
    public int elem;
    public Node next;
    public Node previous;
}
