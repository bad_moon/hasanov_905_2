/*
 * @author Anvar Hasanov
 * 11-905
 * Task #3
 */

package ru.kpfu.itis.group905.hasanov.algorithms.pair1;

public class Account<T extends Number> implements Accountable<T>{
    T id;
    T sum;

    Account (T id, T sum) {
        this.id = id;
        this.sum = sum;
    }

    @Override
    @SuppressWarnings("unchecked")
    public void setSum(int sum) {
        this.sum = (T) (Number) sum;
    }

    @SuppressWarnings("unchecked")
    public void addToSum(T addSum) {
        this.sum = (T) ((Number) (this.sum.doubleValue() + addSum.doubleValue()));
    }

    @SuppressWarnings("unchecked")
    public void subtractFromSum(T subSum) {
        this.sum = (T) ((Number) (this.sum.doubleValue() - subSum.doubleValue()));
    }

    @Override
    public T getId() {
        return this.id;
    }

    @Override
    public T getSum() {
        return this.sum;
    }
}
