package ru.kpfu.itis.group905.hasanov.algorithms.pair3;

import java.util.HashMap;

public class Second {
    public static void main(String[] args) {
        int[] array = new int[] {1, 2, 3, 1, 2, 1, 5, 1, 1};
        HashMap<Integer, Integer> hashMap = new HashMap<>();

        Integer temp;
        for (int num : array) {
            temp = hashMap.get(num);
            hashMap.put(num, temp == null ? temp = 1 : ++temp);
            if (temp > array.length / 2) {
                System.out.println(num);
                break;
            }
        }
    }
}
