package ru.kpfu.itis.group905.hasanov.algorithms.pair3;

import java.util.HashSet;

public class First {
    public static void main(String[] args) {

        int[] array = new int[] {1, 2, 3, 1, 2, 1, 5};
        HashSet<Integer> hashSet = new HashSet<>();

        for (int i : array) {
            hashSet.add(i);
        }

        System.out.println("Count - " + hashSet.size());
    }
}
