package ru.kpfu.itis.group905.hasanov.algorithms.pair10;

import ru.kpfu.itis.group905.hasanov.inf.stack.Node;

public class Task4 {
    public static void main(String[] args) {
        Node<Integer> head = new Node<>(1);
        Node<Integer> last = head;
        for (int i = 2; i <= 10; i++) {
            Node<Integer> node = new Node<>(i);
            last.setNext(node);
            last = node;
        }

        head = reversePair(head);
        last = head;
        while (last != null) {
            System.out.println(last.getElement());
            last = last.getNext();
        }
    }

    public static Node<Integer> reversePair(Node<Integer> head) {
        Node<Integer> temp, node, tp;

        int length = 0;
        temp = head;
        while (temp != null) {
            temp = temp.getNext();
            length++;
        }

        node = head;
        tp = new Node<>(0, head);
        head = tp;
        for (int i = 0; i < length / 4; i++) {
            temp = node.getNext();
            tp.setNext(temp);
            node.setNext(temp.getNext().getNext());

            tp = temp.getNext();
            tp.setNext(node.getNext().getNext());
            node.getNext().setNext(tp);

            temp.setNext(node);
            node = tp.getNext();
        }

        if (length % 4 > 1) {
            temp = node.getNext();
            tp.setNext(temp);
            if (length % 4 == 3) {
                node.setNext(temp.getNext());
                temp.setNext(node);
            } else {
                temp.setNext(node);
                node.setNext(null);
            }
        }

        head = head.getNext();
        return head;
    }
}
