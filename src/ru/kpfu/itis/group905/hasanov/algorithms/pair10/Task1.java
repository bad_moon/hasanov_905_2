package ru.kpfu.itis.group905.hasanov.algorithms.pair10;

import java.util.Arrays;

public class Task1 {
    public static void main(String[] args) {
        int[] array = {3, 1, 2, 1, 6, 1, 3, 6, 2};
        System.out.println(Arrays.toString(minFromLeft(array)));
    }

    public static int[] minFromLeft(int[] array) {
        int[] result = new int[array.length];
        int minIndex = 0;

        for (int i = 0; i < array.length; i++) {
            if (array[i] > array[minIndex]) {
                int j = i;
                while (j > minIndex) {
                    if (array[i] > array[j]) {
                        break;
                    }
                    j--;
                }
                result[i] = array[j];
            } else {
                minIndex = i;
                result[i] = -1;
            }
        }
        return result;
    }
}

