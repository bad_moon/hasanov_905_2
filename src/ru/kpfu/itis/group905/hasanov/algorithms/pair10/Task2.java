package ru.kpfu.itis.group905.hasanov.algorithms.pair10;

import java.util.LinkedList;
import java.util.ListIterator;

public class Task2 {
    public static void main(String[] args) {
        LinkedList<Integer> linkedList = new LinkedList<>();
        linkedList.add(12);
        linkedList.add(12);
        linkedList.add(12);
        linkedList.add(13);
        linkedList.add(13);
        deleteRepetition(linkedList);
        System.out.println(linkedList);
    }

    public static void deleteRepetition(LinkedList<Integer> linkedList) {
        if (linkedList.size() >= 2) {
            ListIterator<Integer> iterator = (ListIterator<Integer>) linkedList.iterator();
            Integer lastCount = iterator.next();
            Integer count;
            while (iterator.hasNext()) {
                count = iterator.next();
                if (count.equals(lastCount)) {
                    iterator.previous();
                    iterator.remove();
                }
                lastCount = count;
            }
        }
    }
}
