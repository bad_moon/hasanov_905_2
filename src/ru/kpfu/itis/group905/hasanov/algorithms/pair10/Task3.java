package ru.kpfu.itis.group905.hasanov.algorithms.pair10;

import ru.kpfu.itis.group905.hasanov.inf.stack.Node;

public class Task3 {
    public static void main(String[] args) {
        Node<Integer> node = new Node<>(1);
        Node<Integer> head = node;
        Node<Integer> temp;
        for (int i = 2; i < 5; i++) {
            temp = new Node<>(i);
            node.setNext(temp);
            node = temp;
        }
        node.setNext(head.getNext());

        System.out.println(task3(head));

    }

    public static int task3(Node<Integer> node) {
        Node<Integer> temp;

        while (node.getNext() != null) {
            temp = node;
            node = node.getNext();
            temp.setNext(null);
        }

        return node.getElement();
    }
}
