/*
 * @author Anvar Hasanov
 * 11-905
 * Task #2
 */

package ru.kpfu.itis.group905.hasanov.algorithms.pair2;

import java.io.BufferedReader;
import java.util.ArrayList;
import java.util.Collections;

public class SecondAlgorithm {
    public static void main(String[] args) {
        int[] array = new int[] {3, 30, 34, 5, 9};

        ArrayList<Integer> list = new ArrayList<>();
        for (int i = 0; i < array.length; i++) {
            list.add(array[i]);
        }

        Comparator comparator = new Comparator();
        list.sort(comparator);

        StringBuilder string = new StringBuilder();
        for (int a : list) {
            string.append(a);
        }
        System.out.println(string);
    }
}
