package ru.kpfu.itis.group905.hasanov.algorithms.pair2;

public class Comparator implements java.util.Comparator<Integer> {

    public int compare(Integer num1, Integer num2) {
        Integer a = Integer.parseInt("" + num1 + num2);
        Integer b = Integer.parseInt("" + num2 + num1);
        return a.compareTo(b) * -1;
    }
}
