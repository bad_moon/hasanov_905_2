/*
 * @author Anvar Hasanov
 * 11-905
 * Task #1
 */

package ru.kpfu.itis.group905.hasanov.algorithms.pair2;

import java.util.Arrays;
import java.util.Scanner;

class FirstAlgorithm {
    static int[] heapSort;
    static int heapCount;

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int k = scanner.nextInt();
        int[] arrSize = new int[k];
        int heapSize = 0;
        for (int i = 0; i < k; i++) {
            arrSize[i] = scanner.nextInt();
            heapSize += arrSize[i];
        }
        heapSort = new int[heapSize];
        int[][] array = new int[k][];
        for (int i = 0; i < k; i++) {
            array[i] = new int[arrSize[i]];
            for (int j = 0; j < arrSize[i]; j++) {
                array[i][j] = scanner.nextInt();
            }
        }
        System.out.println(Arrays.toString(merge(array)));
    }

    public static int[] merge(int[][] inputArray) {

        for (int[] ints : inputArray) {
            for (int anInt : ints) {
                heapAdd(anInt);
            }
        }
        int[] resultArray = new int[heapSort.length];
        for (int i = 0; i < heapSort.length; i++) {
            resultArray[i] = heapDeleteElem();
        }
        return resultArray;
    }

    public static void heapAdd(int num) {
        heapSort[heapCount] = num;
        heapCount++;
        int elem = heapCount - 1;
        while (elem > 0 && heapSort[elem] < heapSort[(elem - 1) / 2]) {
            swap(elem, (elem - 1) / 2);
            elem = (elem - 1) / 2;
        }
    }

    public static void swap(int a, int b) {
        int temp = heapSort[a];
        heapSort[a] = heapSort[b];
        heapSort[b] = temp;
    }

    public static int heapDeleteElem() {
        int deleted = heapSort[0];
        swap(0, heapCount - 1);
        heapCount--;
        int firstIndex = 0;
        int secondIndex = 1;
        if (secondIndex < heapCount - 1
                && heapSort[secondIndex] > heapSort[secondIndex + 1]) {
            secondIndex++;
        }
        while (secondIndex < heapCount && heapCount > 1
                && heapSort[firstIndex] > heapSort[secondIndex]) {
            swap(firstIndex, secondIndex);
            firstIndex = secondIndex;
            secondIndex = 2 * firstIndex + 1;
            if (secondIndex < heapCount - 1
                    && heapSort[secondIndex] > heapSort[secondIndex + 1]) {
                secondIndex++;
            }
        }
        return deleted;
    }
}

