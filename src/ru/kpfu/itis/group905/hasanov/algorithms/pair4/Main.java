/*
 * @author Anvar Hasanov
 * 11-905
 * Task #1 #2 #3
 */


package ru.kpfu.itis.group905.hasanov.algorithms.pair4;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;

public class Main {
    public static void main(String[] args) {
        System.out.println(firstTask(new int[] {1, 2, 3}) + "\n");
        System.out.println(secondTask(new int[] {1, 2, 3}) + "\n");
        System.out.println(thirdTask(new int[] {1, 2, 2, 2}));
    }

    public static String firstTask(int[] inputArray) {
        int[] temp = Arrays.copyOf(inputArray, inputArray.length);
        StringBuilder result = new StringBuilder();
        Arrays.sort(temp);
        result.append(Arrays.toString(temp));
        while (NextPermutation(temp)) {
            result.append(", ").append(Arrays.toString(temp));
        }
        return result.toString();
    }

    public static boolean NextPermutation(int[] array) {
        int i =array.length - 2;
        while (i >= 0 && array[i] >= array[i + 1]) {
            i--;
        }
        if (i < 0) {
            return false;
        }
        int j = array.length - 1;
        while (array[i] >= array[j]) {
            j--;
        }
        swap(array, i, j);
        int a = i + 1;
        int b = array.length - 1;
        while (a < b)
            swap(array, a++, b--);
        return true;
    }

    public static void swap(int[] a, int i, int j) {
        int temp = a[i];
        a[i] = a[j];
        a[j] = temp;
    }

    public static String secondTask(int[] inputArray) {
        StringBuilder result = new StringBuilder("[\n");
        HashSet<String> hashSet = new HashSet<>();
        int[] array = Arrays.copyOf(inputArray, inputArray.length);
        Arrays.sort(array);
        for (int i = 0; i < array.length; i++) {
            hashSet.clear();
            hashSet.add(array[i] + "");
            result.append(hashSet).append(",\n");
            for (int j = i + 1; j < array.length; j++) {
                hashSet.add(array[j] + "");
                result.append(hashSet).append(",\n");
            }
        }
        result.append("]");
        return result.toString();
    }

    public static String thirdTask(int[] inputArray) {
        HashSet<String> variety = new HashSet<>();
        ArrayList<Integer> elements = new ArrayList<>();
        int[] array = Arrays.copyOf(inputArray, inputArray.length);
        Arrays.sort(array);
        StringBuilder result = new StringBuilder();
        result.append("[\n");
        for (int i = 0; i < array.length; i++) {
            if (variety.add(elements.toString())) {
                result.append(elements.toString()).append(",\n");
            }
            elements.clear();
            elements.add(array[i]);
            for (int j = i + 1; j < array.length; j++) {
                if (variety.add(elements.toString())) {
                    result.append(elements.toString()).append(",\n");
                }
                elements.add(array[j]);
            }
        }
        return result.append("]").toString();
    }
}
