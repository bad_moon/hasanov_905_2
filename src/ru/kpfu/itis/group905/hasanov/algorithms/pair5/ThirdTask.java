/*
 * @author Anvar Khasanov
 * 11-905
 * Task #3
 */

package ru.kpfu.itis.group905.hasanov.algorithms.pair5;

import java.util.Scanner;

public class ThirdTask {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String guide = "---\n" + "Enter the number of items " +
                "and the capacity of the backpack.\n" +
                "Then enter the numbers in pairs in the form size " +
                "- value. Example:\n" +
                "4 4\n" + "3 3\n" + "1 1\n" + "2 2\n" + "1 1\n"
                + "The answer is 4\n" + "---\n";
        System.out.println(guide);
        int countItem = scanner.nextInt();
        int capacity = scanner.nextInt();
        int[][] array = new int[capacity + 1][countItem + 1];
        int size, value;
        for (int i = 1; i <= countItem; i++) {
            size = scanner.nextInt();
            value = scanner.nextInt();
            for (int j = 1; j <= capacity; j++) {
                if (size <= j) {
                    array[j][i] = Math.max(array[j][i - 1],
                            array[j - size][i - 1] + value);
                } else {
                    array[j][i] = array[j][i - 1];
                }
            }
        }
        System.out.println("The answer is " + array[capacity][countItem]);
    }
}
