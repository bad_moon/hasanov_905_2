/*
* @author Anvar Khasanov
* 11-905
* Task #1
 */

package ru.kpfu.itis.group905.hasanov.algorithms.pair5;

import java.util.Arrays;
import java.util.Scanner;

public class FirstTask {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter array size: ");
        int[] array = new int[scanner.nextInt()];
        for (int i = 0; i < array.length; i++) {
            array[i] = scanner.nextInt();
        }
        System.out.println(Arrays.toString(maxArray(array)));
    }

    public static int[] maxArray(int[] array) {
        int[] counts = new int[array.length];
        int[] indexes = new int[array.length];

        for (int i = 0; i < array.length; i++) {
            counts[i] = 1;
            indexes[i] = -1;
            for (int j = 0; j <i ; j++) {
                if (array[i] > array[j]) {
                    if (1 + counts[j] > counts[i]) {
                        counts[i] = counts[j] + 1;
                        indexes[i] = j;
                    }
                }
            }
        }
        int maxValue = 0;
        int index = 0;
        for (int i = 0; i < array.length; i++) {
            if (counts[i] > maxValue) {
                maxValue = counts[i];
                index = i;
            }
        }
        int[] result = new int[maxValue];
        result[maxValue - 1] = array[index];

        for (int i = maxValue - 2; i >= 0; i--) {
            result[i] = array[indexes[index]];
            index = indexes[index];
        }
        return result;
    }
}
