/*
 * @author Anvar Khasanov
 * 11-905
 * Task #2
 */

package ru.kpfu.itis.group905.hasanov.algorithms.pair5;

import java.util.Arrays;
import java.util.Scanner;

public class SecondTask {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter first word: ");
        String first = scanner.nextLine();
        System.out.println("Enter second word: ");
        String second = scanner.nextLine();
        System.out.println("Minimum number of steps: "
                + levenstainAlgoritm(first, second));
    }

    public static int levenstainAlgoritm(String firstWord, String secondWord) {
        int[] array = new int[secondWord.length() + 1];
        int[] lastArray;
        for (int i = 0; i <= secondWord.length(); i++) {
            array[i] = i;
        }
        int count;
        for (int i = 1; i <= firstWord.length(); i++) {
            lastArray = Arrays.copyOf(array, array.length);
            array[0] = i;
            for (int j = 1; j <= secondWord.length(); j++) {
                if (firstWord.charAt(i - 1) == secondWord.charAt(j - 1)) {
                    count = 0;
                } else {
                    count = 1;
                }
                array[j] = Math.min(
                        Math.min(lastArray[j] + 1, array[j - 1] + 1),
                        lastArray[j - 1] + count);
            }
        }
        return array[array.length - 1];
    }
}
