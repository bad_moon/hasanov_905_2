package ru.kpfu.itis.group905.hasanov.algorithms.pair9;

public class Task4 {
    public static void main(String[] args) {
        String json = "[\"foo\", {\"bar\":[\"baz\",null,1.0,2]}]";
        System.out.println(formatting(json));
    }

    public static String formatting(String jsonString) {
        StringBuilder result = new StringBuilder();
        StringBuilder tabCount = new StringBuilder();
        char c;
        for (int i = 0; i < jsonString.length(); i++) {
            c = jsonString.charAt(i);
            if (c != ' ') {

                if (c == '}' || c == ']') {
                    tabCount.delete(tabCount.length() - 1, tabCount.length());
                    result.append(tabCount).append(c).append("\n");
                } else if (c == '{' || c == '[') {
                    result.append(tabCount).append(c).append("\n");
                    tabCount.append("\t");
                } else {
                    int j = i;
                    while (j < jsonString.length()) {
                        c = jsonString.charAt(j);
                        if (c == ',') {
                            break;
                        } else if (c == '{' || c == '}' || c == '[' || c== ']') {
                            j--;
                            break;
                        }
                        j++;
                    }
                    result.append(tabCount).append(jsonString, i, j + 1).append("\n");
                    i = j;
                }
            }
        }
        return result.toString();
    }
}

