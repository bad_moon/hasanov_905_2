package ru.kpfu.itis.group905.hasanov.algorithms.pair9;

public class Task2 {
    public static void main(String[] args) {
        String string = "   first   second           third    ";
        System.out.println(reverse(string));
    }

    public static String reverse(String string) {
        StringBuilder result = new StringBuilder();
        string = string.trim();
        for (int i = string.length() - 1; i >= 0; i--) {
            if (string.charAt(i) != ' ') {
                int j = i;
                while (j >= 0 && string.charAt(j) != ' ') {
                    j--;
                }
                result.append(string, j + 1, i + 1).append(" ");
                i = j;
            }
        }
        return result.toString();
    }
}
