package ru.kpfu.itis.group905.hasanov.algorithms.pair9;

import java.math.BigInteger;

public class Task3 {
    public static void main(String[] args) {
        String string = "       14dan54  ";
        System.out.println(atoi(string));
    }

    public static int atoi(String string) {
        string = string.trim();
        int i = 0;
        int result = 0;
        char temp = string.charAt(0);
        while (i + 1 <= string.length() && temp == '+' || temp =='-'
                || (temp >= '0' && temp <= '9')) {
            i++;
            temp = string.charAt(i);
        }
        if (i != 0 && string.charAt(i - 1) != '-') {
            int compare;
            BigInteger bigInteger = new BigInteger(string.substring(0, i));
            if (string.charAt(0) != '-') {
                compare = bigInteger.compareTo(BigInteger.valueOf(Integer.MAX_VALUE));
                if (compare > 0) {
                    return Integer.MAX_VALUE;
                }
            } else {
                compare = bigInteger.compareTo(BigInteger.valueOf(Integer.MIN_VALUE));
                if (compare < 0) {
                    return Integer.MIN_VALUE;
                }
            }
            result = bigInteger.intValue();
        }
        return result;
    }
}
