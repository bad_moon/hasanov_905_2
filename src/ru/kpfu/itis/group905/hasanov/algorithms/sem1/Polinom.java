/*
 * @author Anvar Khasanov
 * 11-905
 * Semester work
 */

package ru.kpfu.itis.group905.hasanov.algorithms.sem1;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.Scanner;

/*
    Polinom - полином, его поле - сташий одночлен (в лексикографическом порядке)
 */

public class Polinom {
    private Monomial head;

    // В конструкторе хранится абсолютный путь до папки (включительно),
    // в котором находится. Добавляет попарно, пока есть значения
    Polinom(String filename) throws FileNotFoundException {
        String path = "src\\ru\\kpfu\\itis\\group905\\hasanov\\algorithms\\sem1\\";
        Scanner scanner = new Scanner(new FileReader(path + filename));
        while (scanner.hasNextInt()) {
            insert(scanner.nextInt(), scanner.nextInt());
        }
    }

    /*
        Добавление одночленов.
        Метод добавляет по следующему принципу: пока степень
        следующего одночлена не больше двигаемся по полиному.
        Если дойдем до конца, добавляем в конец или если степень
        следующего окажется не больше, то
        в таком случае добавляем новый одночлен перед ним
     */
    public void insert(int coefficient, int degree) {
        if (coefficient != 0) {
            Monomial monomial = new Monomial(coefficient, degree);
            if (!headNotNull()) {
                this.head = monomial;
            } else {
                Monomial temp = this.head;
                if (this.head.getDegree() < degree) { // если степень старшего одночлена меньше,
                    monomial.next = temp;             // то добавляем в начало
                    this.head = monomial;
                } else {
                    while (temp.hasNext() && temp.next.getDegree() >= degree) {
                        temp = temp.next;
                    }
                    monomial.next = temp.next;
                    temp.next = monomial;
                    // Так мы добавляем эл. в нужном порядке
                }
            }
        }
    }

    /*
        Приведение подобных членов.
        Двигаемся по полиному начиная со старшего одночлена,
        если степень следующего члена окажется равной степени нынешнего члена,
        то добавляем к нынешнему коэффициенту коэффициент следующего члена.
        Удаляем следующий элемент
     */
    public void combine() {
        if (headNotNull()) {
            Monomial monomial = this.head;
            while (monomial.hasNext()) {
                if (monomial.getDegree() == monomial.next.getDegree()) {
                    monomial.setCoefficient(monomial.getCoefficient()
                            + monomial.next.getCoefficient());
                    deleteNext(monomial);
                } else {
                    monomial = monomial.next;
                }
            }
        }
    }

    // Удаление ссылки на следующий элемент
    private void deleteNext(Monomial monomial) {
        monomial.next = monomial.next.next;
    }

    /*
        Удаляем одночлены с заданной степенью.
        Двигаемся по полиному пока степень следующего одночлена
        меньше заданной. Если находим, то удаляем, иначе выходим из цикла
     */
    public void delete(int degree) {
        if (headNotNull()) {
            Monomial monomial = this.head;
            while (monomial.hasNext()) {
                if (monomial.next.getDegree() == degree) {
                    deleteNext(monomial);
                } else if (monomial.next.getDegree() > degree) {
                    monomial = monomial.next;
                } else {
                    break;
                }
            }
            if (this.head.getDegree() == degree) {
                this.head = this.head.next;  // Проверяем старший одночлен
            }
        }
    }

    /*
        Удаляем одночлены с нечетными коэффициентами.
        Аналогично предыдущему, двигаемся по полиному,
        если коэффициент следующего элемента нечетный,
        то удаляем, иначе двигаемся дальше
     */
    public void deleteOdd() {
        if (headNotNull()) {
            Monomial monomial = this.head;
            while (monomial.hasNext()) {
                if (monomial.next.getCoefficient() % 2 != 0) {
                    deleteNext(monomial);
                } else {
                    monomial = monomial.next;
                }
            }
            if (this.head.getCoefficient() % 2 != 0) {
                this.head = this.head.next;
            }
        }
    }

    /*
        Сумма полиномов.
        Просто добавляем к нашему полиному одночлены, данного полинома.
        После, приводим подобные члены
     */
    public void sum(Polinom polinom) {
        Monomial monomial = polinom.getHead();
        while (monomial != null) {
            insert(monomial.getCoefficient(), monomial.getDegree());
            monomial = monomial.next;
        }
        combine();
    }

    /*
        Производная полинома.
        Первым удаляем все свободные члены,
        затем коэффициент каждого члена умножаем на степень,
        степень уменьшаем.
     */
    public void derivate() {
        if (headNotNull()) {
            Monomial monomial = this.head;
            delete(0);
            while (monomial != null) {
                monomial.setCoefficient(monomial.getCoefficient()
                        * monomial.getDegree());
                monomial.setDegree(monomial.getDegree() - 1);
                monomial = monomial.next;
            }
        }
    }

    /*
        Вычисляет значение полинома в точке x, используя схему Горнера.
        Схема Горнера: https://math1.ru/education/raznoe/gorner.html
     */
    public int value(int x) {
        int result = 0;
        if (headNotNull()) {
            combine();
            int i = this.head.getDegree() - 1;
            result = this.head.getCoefficient();
            Monomial monomial = this.head;
            while (i >= 0) {
                if (monomial.hasNext() && monomial.next.getDegree() >= i) {
                    result = result * x + monomial.next.getCoefficient();
                    monomial = monomial.next;
                } else {
                    result *= x;
                }
                i--;
            }
        }
        return result;
    }

    /*
        Вывод полинома.
        Если нет старшего члена выводим 0.
        Двигаемся по полиному добавляя одночлены
        и + если свободный коэффициент положительный
     */
    @Override
    public String toString() {
        if (!headNotNull()) {
            return "0";
        }
        StringBuilder result = new StringBuilder();
        Monomial temp = this.head;
        result.append(temp);
        while (temp.hasNext()) {
            temp = temp.next;
            result.append(temp.getCoefficient() > 0 ? " + " : " ").append(temp);
        }
        return result.toString();
    }

    private boolean headNotNull() {
        return this.head != null;
    }

    private Monomial getHead() {
        return this.head;
    }
}
