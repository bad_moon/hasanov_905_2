/*
* @author Anvar Khasanov
* 11-905
* Semester work
 */

package ru.kpfu.itis.group905.hasanov.algorithms.sem1;

/*
    Monomial - одночлен, поле класса Polinom. Поля: коэффициент, степень
    и ссылка на следующий одночлен. Класс содержит два метода: toString() и hasNext()
 */

class Monomial {
    private int coefficient;
    private int degree;
    public Monomial next;

    Monomial(int coefficient, int degree) {
        this.coefficient = coefficient;
        this.degree = degree;
    }

    public boolean hasNext() {
        return this.next != null; //Проверяем, есть ли ссылка на следующий элемент
    }

    @Override
    public String toString() {
        StringBuilder result = new StringBuilder();
        if (this.coefficient < 0) {
            result.append("- ");        //Если коэффициент отрицательный добавляем знак минуса
        }

        if (Math.abs(this.coefficient) != 1) {         //Если коэффициент отличен от единицы, то
            result.append(Math.abs(this.coefficient)); //добавляем к результату абсолютное значение коэффициента,
        } else if (this.degree == 0) {                 //иначе если это свободный член (имеет нулевую степень)
            result.append("1");                        //просто добавляем к результату единицу
        }

        if (this.degree != 0 & this.coefficient != 0) { //Если степень и коэффициент отличны от нуля
            result.append("x");                         //добавляем символ х,
            if (this.degree != 1) {                     //если сдепень не равна 1,
                result.append("^").append(this.degree); //то добавляем значение степени
            }
        }
        return result.toString();
    }

    public int getCoefficient() {
        return this.coefficient;
    }

    public void setCoefficient(int coefficient) {
        this.coefficient = coefficient;
    }

    public int getDegree() {
        return this.degree;
    }

    public void setDegree(int degree) {
        this.degree = degree;
    }
}
