/*
 * @author Anvar Khasanov
 * 11-905
 * Semester work
 */

package ru.kpfu.itis.group905.hasanov.algorithms.sem1;

import java.io.FileNotFoundException;

public class Main {
    public static void main(String[] args) throws FileNotFoundException {
        Polinom polinom = new Polinom("input\\in.txt");
        System.out.println(polinom);
        polinom.combine();
        System.out.println("combine:" + "\n" + polinom);
    }
}
