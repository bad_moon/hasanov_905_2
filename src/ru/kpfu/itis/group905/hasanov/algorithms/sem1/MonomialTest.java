/*
 * @author Anvar Khasanov
 * 11-905
 * Semester work
 */

package ru.kpfu.itis.group905.hasanov.algorithms.sem1;

import org.junit.Assert;
import org.junit.Test;

public class MonomialTest {
    @Test
    public void toStringPositiveTest() {
        Monomial monomial = new Monomial(-12, 5);
        String expected = monomial.toString();
        String actual = "- 12x^5";
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void toStringNegativeTest() {
        Monomial monomial = new Monomial(0,0);
        String expected = monomial.toString();
        String actual = "0";
        Assert.assertEquals(expected, actual);
    }
}
