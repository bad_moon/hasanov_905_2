/*
 * @author Anvar Khasanov
 * 11-905
 * Semester work
 */

package ru.kpfu.itis.group905.hasanov.algorithms.sem1;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.Scanner;

public class PolinomTest {
    private Polinom polinom;

    @Before
    public void setUp() throws FileNotFoundException {
        this.polinom = new Polinom("input\\inMasterTest.txt");
    }

    @Test
    public void constructorNegativeTest() throws FileNotFoundException {
        this.polinom = new Polinom("input\\inTestChar.txt");
        String excepted = polinom.toString();
        String actual = (new Polinom("input\\inTestEmpty.txt")).toString();
        Assert.assertEquals(excepted, actual);
    }

    @Test
    public void toStringPositiveTest() {
        String excepted = polinom.toString();
        String actual = "- 9x^3 + 2x^2 - x + 12";
        Assert.assertEquals(excepted, actual);
    }

    @Test
    public void toStringNegativeTest() throws FileNotFoundException {
        this.polinom = new Polinom("input\\inTestEmpty.txt");
        String expected = polinom.toString();
        String actual = "0";
        Assert.assertEquals(expected, actual);
    }
    // ToString уже включает в себя проверку constructorPositive, insertPositive()

    @Test
    public void insertNegativeTest() throws FileNotFoundException{
        Polinom newPolinom = new Polinom("input\\inMasterTest.txt");
        newPolinom.insert(-0, 0);
        String expected = newPolinom.toString();
        String actual = polinom.toString();
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void combinePositiveTest() throws FileNotFoundException {
        Polinom newPolinom = new Polinom("input\\inMasterCombineTest.txt");
        newPolinom.combine();
        String expected = newPolinom.toString();
        String actual = polinom.toString();
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void combineNegativeTest() throws FileNotFoundException {
        this.polinom = new Polinom("input\\inTestEmpty.txt");
        polinom.combine();
        String expected = polinom.toString();
        String actual = "0";
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void deletePositiveTest() throws FileNotFoundException {
        Polinom newPolinom = new Polinom("input\\inMasterCombineTest.txt");
        newPolinom.delete(3);
        newPolinom.combine();
        String expected = newPolinom.toString();
        polinom.delete(3);
        String actual = polinom.toString();
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void deleteNegativeTest() throws FileNotFoundException {
        Polinom newPolinom = new Polinom("input\\inMasterTest.txt");
        newPolinom.delete(12);
        String expected = newPolinom.toString();
        String actual = polinom.toString();
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void deleteOddPositiveTest() throws FileNotFoundException {
        Polinom newPolinom = new Polinom("input\\inOddTest.txt");
        newPolinom.deleteOdd();
        String expected = newPolinom.toString();
        String actual = "0";
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void deleteOddNegativeTest() throws FileNotFoundException {
        Polinom newPolinom = new Polinom("input\\inTestEmpty.txt");
        newPolinom.deleteOdd();
        String expected = newPolinom.toString();
        String actual = "0";
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void sumPositiveTest() throws FileNotFoundException {
        Polinom newPolinom = new Polinom("input\\inMasterTest.txt");
        newPolinom.sum(polinom);
        String expected = newPolinom.toString();
        String path = "src\\ru\\kpfu\\itis\\group905\\hasanov\\algorithms\\sem1\\";
        polinom = new Polinom("input\\inTestEmpty.txt");
        Scanner scanner = new Scanner(new FileReader(path + "input\\inMasterTest.txt"));
        while (scanner.hasNextInt()) {
            polinom.insert(scanner.nextInt() * 2, scanner.nextInt());
        }
        String actual = polinom.toString();
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void sumNegativeTest() throws FileNotFoundException {
        String actual = polinom.toString();
        polinom.sum(new Polinom("input\\inTestEmpty.txt"));
        String expected = polinom.toString();
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void derivatePositiveTest() {
        polinom.derivate();
        String expected = polinom.toString();
        String actual = "- 27x^2 + 4x - 1";
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void derivateNegativeTest() throws FileNotFoundException {
        polinom = new Polinom("input\\inTestEmpty.txt");
        polinom.derivate();
        String expected = polinom.toString();
        String actual = "0";
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void valuePositiveTest() throws FileNotFoundException {
        int expected = polinom.value(2);
        int actual = -54;
        Assert.assertEquals(expected, actual);
        polinom = new Polinom("input\\inValueTest.txt");
        expected = polinom.value(2);
        actual = 8416;
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void valueNegativeTest() throws FileNotFoundException {
        polinom = new Polinom("input\\inTestEmpty.txt");
        int expected = polinom.value(2);
        int actual = 0;
        Assert.assertEquals(expected, actual);
    }
}
