package ru.kpfu.itis.group905.hasanov.algorithms.pair7;

import java.util.Scanner;

public class FirstTask {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
//        int input = scanner.nextInt();
        int input = 15255;
        int[] money = {5000, 1000, 500, 100, 50};
        int result = 0;
        for (int value : money) {
            result += input / value;
            input %= value;
        }
        System.out.println(result);
    }
}
