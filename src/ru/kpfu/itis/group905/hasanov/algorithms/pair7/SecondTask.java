package ru.kpfu.itis.group905.hasanov.algorithms.pair7;

public class SecondTask {
    public static void main(String[] args) {
        int[] array = {-2, 1, -3, 4, -1, 2, 1, -5, 4};
        System.out.println(maxSubarray(array));
    }

    public static int maxSubarray(int[] array) {
        int[] sumArray = new int[array.length];
        int result, temp;

        int max = array.length - 1;
        for (int i = array.length - 2; i > 0; i--) {
            sumArray[i] = sumArray[i + 1] + array[i];
            if (sumArray[i] > sumArray[max]) {
                max = i;
            }
        }

        result = array[max];
        for (int i = max + 1; i < array.length; i++) {
            if (array[i] >= 0) {
                result += array[i];
            } else {
                temp = array[i];
                i++;
                while (i < array.length) {
                    temp += array[i];
                    if (temp >= 0) {
                        result += temp;
                        break;
                    }
                    i++;
                }
            }
        }

        return result;
    }
}
