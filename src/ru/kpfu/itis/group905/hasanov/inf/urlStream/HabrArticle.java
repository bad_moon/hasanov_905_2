package ru.kpfu.itis.group905.hasanov.inf.urlStream;

import java.io.Serializable;

public class HabrArticle implements Serializable {
    private String title;
    private String sammari;
    private String url;

    public HabrArticle(String title, String sammari, String url) {
        this.title = title;
        this.sammari = sammari;
        this.url = url;
    }

    @Override
    public String toString() {
        return title + "\n" + sammari + "\n" + url;
    }

    public String getTitle() {
        return title;
    }
}
