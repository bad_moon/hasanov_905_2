package ru.kpfu.itis.group905.hasanov.inf.urlStream;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class HabrArticlesParser {
    private static ByteArrayOutputStream byteStream = new ByteArrayOutputStream();
    private static String habrFeedUrl = "https://habr.com/ru/top/";
    private static InputStream inputStream;


    public static ArrayList<HabrArticle> parsing(int count) throws IOException {
        String articleHead = "<article class=\"post post_preview\" lang=\"ru\">";
        Pattern pattern = Pattern.compile(".*<h2 class=\"post__title\">" +
                "<a href=\"([\\w:/.\\- ]*)\" class=\"post__title_link\">(.*)" +
                "</a></h2>.*<div class=\"post__text post__text-html[ ]{2}post__text_v1 \">" +
                "(.*)</div>.*<a class=\"btn btn_x-large btn_outline_blue " +
                "post__habracut-btn\" href=\".*\">.*");

        inputStream = openStream(getRootWithPage(1));

        ArrayList<HabrArticle> articlesList = new ArrayList<>();
        int pageNum = 1, articlesCount = 0, c;
        String line;

        while (articlesCount < count) {
            c = inputStream.read();

            if (c == -1) {
                try {
                    // If page is over, go to another page
                    pageNum++;
                    inputStream = openStream(getRootWithPage(pageNum));
                } catch (FileNotFoundException e) {
                    // If page not found, break
                    break;
                }
                continue;
            }

            if (c == '\n') {
                line = byteStream.toString().trim();
                byteStream.reset();

                if (line.equals(articleHead)) {
                    String article = readArticle();

                    Matcher matcher = pattern.matcher(article);
                    if (matcher.matches()) {
                        String body = matcher.group(3).replace("<br>", "\n");
                        HabrArticle habrArticle = new HabrArticle(matcher.group(2),
                                body, matcher.group(1));

                        articlesList.add(habrArticle);

                        articlesCount++;
                    }
                }
            } else {
                byteStream.write(c);
            }
        }
        return articlesList;
    }

    private static String readArticle() throws IOException {
        StringBuilder article = new StringBuilder();
        String line = "";
        int ch;
        while (!line.equals("</article>")) {
            ch = inputStream.read();
            byteStream.write(ch);
            if (ch == '\n') {
                line = byteStream.toString().trim();
                byteStream.reset();
                article.append(line);
            }
        }
        return article.toString();
    }

    private static InputStream openStream(String url) throws IOException {
        return new URL(url).openStream();
    }

    private static String getRootWithPage(int pageNumber) {
        return habrFeedUrl + "page" + pageNumber + "/?page=" + pageNumber;
    }

    public static void setHabrFeedUrl(String habrFeedUrl) {
        HabrArticlesParser.habrFeedUrl = habrFeedUrl;
    }

    public static String getHabrFeedUrl() {
        return habrFeedUrl;
    }
}
