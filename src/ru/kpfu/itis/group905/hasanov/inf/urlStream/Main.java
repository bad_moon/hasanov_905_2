package ru.kpfu.itis.group905.hasanov.inf.urlStream;

import java.io.*;
import java.util.ArrayList;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) throws IOException {
        Scanner scanner = new Scanner(System.in);
        String path = "src\\ru\\kpfu\\itis\\group905\\hasanov\\inf\\urlStream\\";

        System.out.println("Enter count of articles:");
        int count = scanner.nextInt();
        ArrayList<HabrArticle> habrArticles = HabrArticlesParser.parsing(count);

        //writing
        try (ObjectOutputStream outputStream = new ObjectOutputStream(
                new FileOutputStream(path.concat("articles.txt")))) {
            outputStream.writeObject(habrArticles);
        }

        ArrayList<HabrArticle> habrArticlesFromFile;

        //reading and print
        try (ObjectInputStream inputStream = new ObjectInputStream(
                new FileInputStream(path.concat("articles.txt")))) {
            habrArticlesFromFile = (ArrayList<HabrArticle>) inputStream.readObject();

            for (HabrArticle article : habrArticlesFromFile) {
                System.out.println(article.getTitle());
            }

        } catch (ClassNotFoundException | ClassCastException e) {
            e.printStackTrace();
        }


    }
}
