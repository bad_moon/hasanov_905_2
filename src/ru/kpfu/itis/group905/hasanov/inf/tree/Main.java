package ru.kpfu.itis.group905.hasanov.inf.tree;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int count = scanner.nextInt();
        int[] array = new int[count];
        for (int i = 0; i < count; i++) {
            array[i] = scanner.nextInt();
        }
        MyTree myTree = new MyTree(array);
        System.out.println(myTree.countDifNum());
        System.out.println(myTree.sumAllLevelsMax());
    }
}