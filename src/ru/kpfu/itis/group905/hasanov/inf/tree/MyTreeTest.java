package ru.kpfu.itis.group905.hasanov.inf.tree;

import org.junit.Assert;
import org.junit.Test;
import java.util.HashMap;
import java.util.Map;

public class MyTreeTest {
    @Test
    public void PositiveCountDifNum() {
        int[] array = new int[] {-1, -2, -4, 0, 1, 0, 2, 6, 2};

        MyTree myTree = new MyTree(array);
        Map<Integer, Integer> expected = myTree.countDifNum();

        Map<Integer, Integer> actual = new HashMap<>();
        Integer value;
        for (int item : array) {
            if (item > 0) {
                value = actual.get(1);
                actual.put(1, (value == null ? 1 : value + 1));
            } else if (item < 0) {
                value = actual.get(-1);
                actual.put(-1, (value == null ? 1 : value + 1));
            } else {
                value = actual.get(0);
                actual.put(0, (value == null ? 1 : value + 1));
            }
        }
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void NegativeCountDifNum() {
        int[] array = new int[] {};

        MyTree myTree = new MyTree(array);
        Map<Integer, Integer> expected = myTree.countDifNum();

        Map<Integer, Integer> actual = new HashMap<>();
        for (int i = -1; i < 2; i++) {
            actual.put(i, 0);
        }

        Assert.assertEquals(expected, actual);
    }

    @Test
    public void PositiveSumAllLevelsMax() {
        int[] array = new int[] {1, 2, 3, 4, 5, 6, 7, 8, 9};

        MyTree myTree = new MyTree(array);
        int expected = myTree.sumAllLevelsMax();

        int actual = 29;

        Assert.assertEquals(expected, actual);
    }

    @Test
    public void NegativeSumAllLevelsMax() {
        int[] array = new int[] {};

        MyTree myTree = new MyTree(array);
        int expected = myTree.sumAllLevelsMax();

        int actual = 0;

        Assert.assertEquals(expected, actual);
    }
}