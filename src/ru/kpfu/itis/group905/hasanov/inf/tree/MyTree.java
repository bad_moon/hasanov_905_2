package ru.kpfu.itis.group905.hasanov.inf.tree;

import java.util.*;

public class MyTree {
    private Node root;

    MyTree(int[] inputArray) {
        int[] array = Arrays.copyOf(inputArray, inputArray.length);
        Arrays.sort(array);
        this.root = createMyTree(0, array.length - 1, array);
    }

    private Node createMyTree(int start, int end, int[] array) {
        if (start > end) {
            return null;
        }
        int middle = (start + end) / 2;
        Node node = new Node(array[middle]);
        node.left = createMyTree(start, middle - 1, array);
        node.right = createMyTree(middle + 1, end, array);
        return node;
    }

    public Map<Integer, Integer> countDifNum() {
        Map<Integer, Integer> map = new HashMap<>();
        for (int i = -1; i <= 1; i++) {
            map.put(i, 0);
        }
        toMapReq(this.root, map);
        return map;
    }

    private void toMapReq(Node node, Map<Integer, Integer> map) {
        if (node != null) {
            int count;
            if (node.data > 0) {
                count = map.get(1);
                map.put(1, count + 1);
            } else if (node.data < 0) {
                count = map.get(-1);
                map.put(-1, count + 1);
            } else {
                count = map.get(0);
                map.put(0, count + 1);
            }
            toMapReq(node.left, map);
            toMapReq(node.right, map);
        }
    }

    public int sumAllLevelsMax() {
        if (this.root == null) {
            return 0;
        }
        Queue<Node> levels = new LinkedList<>();
        levels.add(this.root);
        int max = 0;
        int sum = this.root.data;
        int nodes = 2;
        for (int i = 0; !levels.isEmpty(); i++) {
            Node node = levels.remove();
            if (i <= nodes) {
                max = Math.max(node.data, max);
            } else {
                sum += max;
                max = node.data;
                i = 0;
                nodes *= 2;
            }
            if (node.left != null) {
                levels.add(node.left);
            }
            if (node.right!= null) {
                levels.add(node.right);
            }
        }
        sum += max;
        return sum;
    }

    private static class Node {
        private int data;
        private Node right;
        private Node left;

        Node(int data) {
            this.data = data;
            this.right = null;
            this.left = null;
        }

        public int getData() {
            return data;
        }
    }
}
