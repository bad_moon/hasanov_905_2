package ru.kpfu.itis.group905.hasanov.inf.kr1;

public class Node<T> implements Comparable<T>{
    public T element;
    public Node<T> next;

    @Override
    public int compareTo(T SecondElement) {
        String elemStr = this.element.toString();
        String secElemStr = SecondElement.toString();
        if (elemStr.length() < secElemStr.length()) {
            return -1;
        } else if (elemStr.length() > secElemStr.length()) {
            return 1;
        }
        return 0;
    }
}
