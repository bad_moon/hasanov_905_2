package ru.kpfu.itis.group905.hasanov.inf.kr1;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.Collections;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) throws FileNotFoundException, StackIsEmptyException {

        Stack<String> stack = new Stack<>();
        String path = "src\\ru\\kpfu\\itis\\group905\\hasanov\\inf\\kr1\\";
        Scanner scanner = new Scanner((new File(path + "in.txt")));
        while (scanner.hasNext()) {
            stack.push(scanner.next());
        }
        scanner.close();

        Comparator<String> comparator = new Comparator<>();

        System.out.println(stack.pop("Ruslan"));
        System.out.println(stack.isEmpty());
        System.out.println(stack.size());
//        stack.clear();
//        Collections.sort(stack, comparator);

        PrintWriter printWriter = new PrintWriter(path + "out.txt");
        printWriter.println(stack);
        printWriter.close();
    }
}
