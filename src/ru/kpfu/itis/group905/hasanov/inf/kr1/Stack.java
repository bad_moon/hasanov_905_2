package ru.kpfu.itis.group905.hasanov.inf.kr1;

import java.util.*;

public class Stack<T> extends LinkedList<T>{
    private Node<T> first;
    private int size;
    private int count;

    Stack() {}

    @Override
    public void push(T elem) {
        Node<T> newNode = new Node<>();
        newNode.element = elem;
        newNode.next = this.first;
        this.first = newNode;
        size++;
    }

    public T pop(T elem) throws StackIsEmptyException {
        if (elem.equals(this.first.element)) {
            T result = this.first.element;
            this.first = this.first.next;
            size--;
            return result;
        }
        throw new StackIsEmptyException();
    }

    @Override
    public String toString() {
        StringBuilder result = new StringBuilder();
        Node<T> node = this.first;

        for (int i = 0; i < size; i++) {
            result.append(node.element).append("\n");
            node = node.next;
        }
        return result.toString();
    }

    @Override
    public Object[] toArray() {
        Object[] array = new Object[this.size];
        Node<T> node = this.first;
        for (int i = 0; i < this.size; i++) {
            array[i] = node;
            node = node.next;
        }
        System.out.println(Arrays.toString(array));
        return array;
    }

    @Override
    public boolean isEmpty() {
        return this.first != null;
    }

    @Override
    public int size() {
        return this.size;
    }

    @Override
    public void clear() {
        this.first = null;
        this.size = 0;
    }
}


