/*
 * @author Anvar Hasanov
 * 11-905
 * Task #2
 */

package ru.kpfu.itis.group905.hasanov.inf.exceptions;

import java.util.Scanner;

public class MainPoint {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int x, y, x1, y1;
        Point point;
        while (true) {
            System.out.println("Enter x coordinate of current point");
            x = scanner.nextInt();
            System.out.println("Enter y coordinate of current point:");
            y = scanner.nextInt();
            point = new Point(x, y);
            System.out.println("Enter x coordinate of target point:");
            x1 = scanner.nextInt();
            System.out.println("Enter y coordinate of target point:");
            y1 = scanner.nextInt();
            if (x1 - x == 0) {
                System.out.println("Zero is an invalid denominator. Please try again.");
            }
            else {
                System.out.println(point.slope(x1, y1));
                break;
            }
        }
    }
}
