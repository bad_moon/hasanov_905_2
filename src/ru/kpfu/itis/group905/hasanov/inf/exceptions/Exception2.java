package ru.kpfu.itis.group905.hasanov.inf.exceptions;

import java.io.*;


/*
    1) В сигнатуре метода не указан ExceptionOfMine,
       Программа должна выбросить исключение Exception2, при этом выведется еще - c,
       т.к. finally выполниться в любом случае
       Может до ошибки, может после, зависит от буфера.

    2) Программа вывела все как предполагалось.
    3) Исправления: В сигнатуре метода указал какое исключение может выбросить метод.
 */

public class Exception2 extends Exception {
    public static void main(String[] args) throws Exception2 {
        try {
            throw new Exception2();
        } finally {
            System.out.println("c");
        }
    }
}
