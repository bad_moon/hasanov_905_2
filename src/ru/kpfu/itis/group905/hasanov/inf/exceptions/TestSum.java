/*
 * @author Anvar Hasanov
 * 11-905
 * Task #4
 */

package ru.kpfu.itis.group905.hasanov.inf.exceptions;

public class TestSum {
    public static boolean test(int firstNumber, int secondNumber) {
        try {
            if (firstNumber + secondNumber > 100) {
                throw new SumException();
            }
        } catch (SumException e) {
            System.out.println("The number is greater than 100");
            System.exit(1);
        }
        return true;
    }
}
