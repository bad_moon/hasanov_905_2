/*
 * @author Anvar Hasanov
 * 11-905
 * Task #5
 */


package ru.kpfu.itis.group905.hasanov.inf.exceptions;

import java.io.IOException;

public class RunnerProcessor {
    public static int execute(final IBrokenRunner runner) throws IOException {
        try {
            return runner.run();
        } catch (IOException e) {
            throw new IOException();
        } catch (Exception e) {
            return -1;
        } catch (Throwable e) {
            return 0;
        }
    }
}
