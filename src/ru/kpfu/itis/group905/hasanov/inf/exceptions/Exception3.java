package ru.kpfu.itis.group905.hasanov.inf.exceptions;

import java.io.*;

/*
    1) Ошибок в написании программы не видно, попадаем в try,
       выводится - а, бросаем исключение и перехватываем ее, попадаем в catch (NullPointerException e)
       выводится - b, бросаем исключение Exception. метод catch (Exception e) бесполезен, ведь мы можем попасть в
       catch только из try.

    2) Программа вывела все как предполагалось.
    3) Исправлений нет.
 */

public class Exception3 {
    public static void main(String[] args) throws Exception {
        try {
            System.out.println("a");
            throw new NullPointerException();
        } catch (NullPointerException e) {
            System.out.println("b");
            throw new Exception();
        } catch (Exception e) {
            System.out.println("c");
        }
    }
}
