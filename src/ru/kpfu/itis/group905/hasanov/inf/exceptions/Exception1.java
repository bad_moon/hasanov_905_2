package ru.kpfu.itis.group905.hasanov.inf.exceptions;

import java.io.*;

/*
    1) Первым делом необходимо унаследоваться от Exception.
    Тогда программа заработает. Код в блоке try бросит исключение,
    после чего перейдет к catch (Exception1 e) выведет - b.
    Код в блоке finally выполняется в любом случае, выведет - d
    Итого программа выведет - bd

    2) Программа вывела все как предполагалось.
    3) Исправления: наследуемся от Exception.
 */

public class Exception1 extends Exception {
    public static void main(String[] args) {
        try {
            throw new Exception1();
        } catch (Exception1 e) {
            System.out.println("b");
        } catch (Exception e) {
            System.out.println("c");
        } finally {
            System.out.println("d");
        }
    }
}
