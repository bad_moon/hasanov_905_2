/*
 * @author Anvar Hasanov
 * 11-905
 * Task #1
 */

package ru.kpfu.itis.group905.hasanov.inf.exceptions;

import java.util.Scanner;

public class IDNumber {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String text = scanner.next();
        try {
            double chk = (double) Integer.parseInt(text) / 1000000000L;
            if (chk >= 1 && chk <= 2) {
                System.out.println("correct");
            }
            else {
                System.out.println("incorrect");
            }
        } catch (NumberFormatException e) {
            System.out.println("incorrect");
        }
    }
}

