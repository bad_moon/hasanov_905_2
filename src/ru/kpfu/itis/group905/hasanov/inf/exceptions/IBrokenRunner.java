/*
 * @author Anvar Hasanov
 * 11-905
 * Task #5
 */

package ru.kpfu.itis.group905.hasanov.inf.exceptions;

import java.io.IOException;

public class IBrokenRunner {
    public int run() throws IOException {
        throw new IOException();
    }
}
