/*
 * @author Anvar Hasanov
 * 11-905
 * Task #6
 */

package ru.kpfu.itis.group905.hasanov.inf.exceptions;

public class ThrowsExceptions {
    public static void methodThrowsClassCastException() {
        Object obj = new Point(1, 2);
        Double num = (Double) obj;
    }

    public static void methodThrowsNullPointerException() {
        String str = null;
        str.length();
    }
}
