/*
 * @author Anvar Hasanov
 * 11-905
 * Task #3
 */

package ru.kpfu.itis.group905.hasanov.inf.exceptions;

import java.util.InputMismatchException;
import java.util.Scanner;

public class Convert {
    public static void main(String[] args) {
        Scanner scanner;
        int a, b;
        double res;
        while (true) {
            scanner = new Scanner(System.in);
            try {
                System.out.print("Enter height in feet:");
                a = scanner.nextInt();
                System.out.print("and in inches:");
                b = scanner.nextInt();
                if (a < 0 || b < 0) {
                    System.out.println("Please enter positive values only.");
                    continue;
                }
                res = a * 30.48 + b * 2.54;
                System.out.println("Result:" + res + " cm");
                break;
            } catch (NumberFormatException | InputMismatchException e) {
                System.out.println("You must enter integers. Please try again.");
            }
        }

    }
}
