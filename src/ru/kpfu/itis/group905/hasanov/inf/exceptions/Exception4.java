package ru.kpfu.itis.group905.hasanov.inf.exceptions;

import java.io.*;

/*
    1) Ошибок в написании программы не видно. Выводится - а, попадаем в try,
       выводится - b, ошибок нет попадаем в finally, выводится - d, выходим выводится - e.
       Итог: abde.

    2) Программа вывела все как предполагалось.
    3) Исправлений нет.
 */

public class Exception4 {
    public static void main(String[] args) {
        System.out.println("a");
        try {
            System.out.println("b");
        } catch (Exception e) {
            System.out.println("c");
        } finally {
            System.out.println("d");
        }

        System.out.println("e");
    }
}
