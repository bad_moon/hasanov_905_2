package ru.kpfu.itis.group905.hasanov.inf.exceptions;

/*
    1) Ошибок в написании программы не видно. Не понятно зачем программа наследуется от Exception.
       Выводим - Hi!, попадаем в try, выводим - а,
       бросаем исключение Exception6 (как название нашего класса, но это не должно ни на что повлиять),
       перехватываем его и выводим - b, попадаем в finally и выводим - d, и опять не нужен catch (Exception e).
       Выводинся - е.
       Итог: Hi! a b d e

    2) Программа вывела все как предполагалось.
    3) Исправлений нет.
 */

public class Exception6 extends Exception {
    public static void main(String[] args) {
        System.out.println("Hi!");
        try {
            System.out.println("a");
            throw new Exception6();
        } catch (Exception6 e) {
            System.out.println("b");
        } catch (Exception e) {
            System.out.println("c");
        } finally {
            System.out.println("d");
        }

        System.out.println("e");
    }
}
