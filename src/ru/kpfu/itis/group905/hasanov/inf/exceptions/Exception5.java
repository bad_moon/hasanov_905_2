package ru.kpfu.itis.group905.hasanov.inf.exceptions;

import java.io.*;


/*
    1) Ошибок в написании программы не видно. Вызывается ветод divide, попадаем в try,
       11/1=11, собираемя вернуть 11, попадаем в finally и перезаписываем возвращаемое значение на 10.
       Выведется - 10.

    2) Программа вывела все как предполагалось.
    3) Исправлений нет.
 */

public class Exception5 {
    public static int divide(int a, int b) {
        try {
            return a/b;
        } catch (Exception e) {
            return 0;
        } finally {
            return 10;
        }
    }

    public static void main(String[] args) {
        int i = divide(11, 1);
        System.out.println(i);
    }
}
