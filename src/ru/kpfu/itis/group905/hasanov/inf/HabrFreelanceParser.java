package ru.kpfu.itis.group905.hasanov.inf;

import java.io.*;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class HabrFreelanceParser {
    static String habrFreelanceRoot = "https://freelance.habr.com";
    static String habrTasksUrl = "https://freelance.habr.com/tasks";

    public static void main(String[] args) throws IOException {
        Scanner scanner = new Scanner(System.in);
        ArrayList<Task> tasks;
        System.out.println("\t... Habr work parser v12.093 ...");
        System.out.println("Take vacancies to work? [y/n]");
        String input = scanner.next();
        if (input.equals("y")) {
            tasks = getTasks();

            String tasksToStr = toString(tasks);
            System.out.println(tasksToStr + "\n");

            System.out.println("Save tasks to file? [y/n]");
            input = scanner.next();
            if (input.equals("y")) {
                printToFile(tasksToStr);
            }
        }
        System.out.println("Enter anything to exit...");
        scanner.next();
    }

    public static void printToFile(String tasks) throws FileNotFoundException {
        PrintWriter printWriter = new PrintWriter(
                new File("").getAbsolutePath() + "/output.txt");
        printWriter.println(tasks);
        printWriter.close();
    }

    public static String toString(ArrayList<Task> taskArrayList) {
        StringBuilder result = new StringBuilder();
        for (Task task : taskArrayList) {
            result.append(task).append("\n");
        }
        return result.toString();
    }

    public static ArrayList<Task> getTasks() throws IOException {
        URL url = new URL(habrTasksUrl);
        ArrayList<Task> tasksList = new ArrayList<>();
        URLConnection connection = url.openConnection();
        InputStream inputStream = connection.getInputStream();
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        Pattern pattern = Pattern.compile("<article class=\"task task_list\">.*[^<]" +
                "<div class=\"task__title\" title=\"(.*[^\"])\"><a href=\"(/tasks/[0-9]*[^\"])\">.*" +
                "<span class='count'>([.[^<]]*)\\. <span class='suffix'>([.[^<]]*)</span></span>.*");
        Matcher matcher;
        int b;
        while ((b = inputStream.read()) != -1) {
            if ((char)b != '\n') {
                byteArrayOutputStream.write(b);
            } else {
                String inputLine = byteArrayOutputStream.toString();
                matcher = pattern.matcher(inputLine);
                if (matcher.matches()) {
                    Task task = new Task(matcher.group(1), matcher.group(3) + " "
                            + matcher.group(4), habrFreelanceRoot + matcher.group(2));
                    tasksList.add(task);
                }
                byteArrayOutputStream.reset();
            }
        }
        byteArrayOutputStream.close();
        inputStream.close();
        return tasksList;
    }

    private static class Task {
        private String service;
        private String payment;
        private String url;

        public Task(String service, String payment, String url) {
            this.service = service;
            this.payment = payment;
            this.url = url;
        }

        @Override
        public String toString() {
            return service + "\t - \t" + payment + "\t" + url;
        }
    }
}