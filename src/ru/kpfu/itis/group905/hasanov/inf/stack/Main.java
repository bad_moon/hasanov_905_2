package ru.kpfu.itis.group905.hasanov.inf.stack;

import ru.kpfu.itis.group905.hasanov.inf.kr1.StackIsEmptyException;

public class Main {
    public static void main(String[] args) throws StackIsEmptyException {
        Stack<Integer> stack = new Stack<>();
        for (int i = 1; i < 6; i++) {
            stack.push(i);
        }
        System.out.println(stack);
        System.out.println(stack.size());
        System.out.println(stack.peek());
        for (int i = 1; i < 6; i++) {
            System.out.println(stack.pop());
        }
        System.out.println(stack.size());
    }
}
