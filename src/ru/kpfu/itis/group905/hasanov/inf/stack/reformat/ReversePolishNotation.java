package ru.kpfu.itis.group905.hasanov.inf.stack.reformat;

import ru.kpfu.itis.group905.hasanov.inf.kr1.StackIsEmptyException;

public class ReversePolishNotation {
    public static void main(String[] args) throws StackIsEmptyException {

        String input = "3 + 4 * 2 / ( 1 - 5 ) ^ 2";
        String[] inArr = input.split(" ");

        System.out.println(calculateValue(polishNotation(inArr)));
    }

    public static Stack<StackElement> polishNotation(String[] inputArr) throws StackIsEmptyException {
        Stack<StackOperation> operations = new Stack<>();
        Stack<StackElement> notation = new Stack<>();

        for (int i = 0; i < inputArr.length; i++) {
            if (inputArr[i].charAt(0) <= '9' && inputArr[i].charAt(0) >= '0') {
                notation.push(new StackNumber(Double.parseDouble(inputArr[i])));
            } else {
                if (inputArr[i].equals("(")) {
                    int length = 0;
                    i++;
                    while (!inputArr[i + length].equals(")")){
                        length++;
                    }
                    String[] temp = new String[length];
                    System.arraycopy(inputArr, i, temp, 0, length);
                    Stack<StackElement> newNotation = polishNotation(temp);
                    Stack<StackElement> tempStack = new Stack<>();
                    while (!newNotation.isEmpty()) {
                        tempStack.push(newNotation.pop());
                    }
                    while (!tempStack.isEmpty()) {
                        StackElement elem = tempStack.pop();
                        if (elem instanceof StackNumber) {
                            notation.push(elem);
                        } else {
                            notation.push(new StackOperation(elem.toString(),
                                    notation));
                        }
                    }
                    i += length;
                } else {
                    StackOperation operation = new StackOperation(inputArr[i], notation);
                    if (operation.isPushNeeded(operations)) {
                        notation.push(operations.pop());
                    }
                    operations.push(operation);
                }
            }
        }
        while (!operations.isEmpty()) {
            notation.push(operations.pop());
        }
        return notation;
    }

    public static double calculateValue(Stack<StackElement> stack) throws StackIsEmptyException {
        return stack.peek().value();
    }
}
