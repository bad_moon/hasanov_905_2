package ru.kpfu.itis.group905.hasanov.inf.stack.reformat;

import ru.kpfu.itis.group905.hasanov.inf.kr1.StackIsEmptyException;

public class SumOperation extends StackOperation {
    protected Stack<StackElement> stack;

    SumOperation(Stack<StackElement> stack) {
        this.stack = stack;
    }

    @Override
    public String toString() {
        return "+";
    }

    @Override
    public double value() throws StackIsEmptyException {
        stack.pop();

        StackElement firstElem = this.stack.pop();
        this.stack.peek().value();
        stack.push(firstElem);
        firstElem.value();

        double res = stack.pop().value() + stack.pop().value();

        stack.push(new StackNumber(res));
        return res;
    }

    public void setStack(Stack<StackElement> stack) {
        this.stack = stack;
    }
}
