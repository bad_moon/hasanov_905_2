package ru.kpfu.itis.group905.hasanov.inf.stack;

public class Flat implements Comparable<Flat>{
    private String name;
    private double height;
    private double weight;
    private Double area;

    Flat(String name, double height, double weight) {
        this.name = name;
        this.height = height;
        this.weight = weight;
        area = height * weight;
    }

    @Override
    public String toString() {
        return "name: " + name + " weight: " + weight + " height: " + height;
    }

    @Override
    public int compareTo(Flat second) {
        return this.area.compareTo(second.area);
    }

    public String getName() {
        return name;
    }

    public double getHeight() {
        return height;
    }

    public double getWeight() {
        return weight;
    }
}
