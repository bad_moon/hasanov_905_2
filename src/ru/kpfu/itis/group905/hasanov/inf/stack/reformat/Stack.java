package ru.kpfu.itis.group905.hasanov.inf.stack.reformat;

import ru.kpfu.itis.group905.hasanov.inf.kr1.StackIsEmptyException;
import ru.kpfu.itis.group905.hasanov.inf.stack.Node;

import java.util.Collection;
import java.util.Iterator;

public class Stack<T extends StackElement> implements Collection<T> {
    private Node<T> headElement;
    private int size = 0;

    public void push(T element) {
        Node<T> node = new Node<>(element);
        node.setNext(headElement);
        headElement = node;
        size++;
    }

    public T pop() throws StackIsEmptyException {
        if (isEmpty()) {
            throw new StackIsEmptyException();
        }

        T count = headElement.getElement();
        headElement = headElement.getNext();
        size--;

        return count;
    }

    public T peek() throws StackIsEmptyException {
        if (isEmpty()) {
            throw new StackIsEmptyException();
        }

        return headElement.getElement();
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public boolean isEmpty() {
        return size == 0;
    }

    @Override
    public void clear() {
        headElement = null;
        size = 0;
    }

    @Override
    public String toString() {
        Node<T> node = headElement;

        StringBuilder result = new StringBuilder();
        while (node != null) {
            result.append(node.getElement());
            result.append(" ");
            node = node.getNext();
        }

        return result.toString();
    }

    @Override
    public boolean remove(Object o) {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean contains(Object o) {
        throw new UnsupportedOperationException();
    }

    @Override
    public Iterator<T> iterator() {
        throw new UnsupportedOperationException();
    }

    @Override
    public Object[] toArray() {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean add(T t) {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean addAll(Collection c) {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean retainAll(Collection c) {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean removeAll(Collection c) {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean containsAll(Collection c) {
        throw new UnsupportedOperationException();
    }

    @Override
    public T[] toArray(Object[] a) {
        throw new UnsupportedOperationException();
    }
}
