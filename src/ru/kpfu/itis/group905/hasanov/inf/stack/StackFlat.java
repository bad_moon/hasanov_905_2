package ru.kpfu.itis.group905.hasanov.inf.stack;

import ru.kpfu.itis.group905.hasanov.inf.kr1.StackIsEmptyException;
import java.util.NoSuchElementException;
import java.util.ListIterator;

import java.util.*;

public class StackFlat<T extends Flat> implements List<T> {
    private Node<T> headElement;
    private int size = 0;

    public void push(T element) {
        Node<T> node = new Node<>(element);
        node.setNext(headElement);
        headElement = node;
        size++;
    }

    public T pop() throws StackIsEmptyException {
        if (isEmpty()) {
            throw new StackIsEmptyException();
        }

        T count = headElement.getElement();
        headElement = headElement.getNext();
        size--;

        return count;
    }

    public T peek() throws StackIsEmptyException {
        if (isEmpty()) {
            throw new StackIsEmptyException();
        }

        return headElement.getElement();
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public boolean isEmpty() {
        return size == 0;
    }

    @Override
    public void clear() {
        headElement = null;
        size = 0;
    }

    @Override
    public String toString() {
        Node<T> node = headElement;

        StringBuilder result = new StringBuilder();
        while (node != null) {
            result.append(node.getElement());
            result.append("\n");
            node = node.getNext();
        }

        return result.toString();
    }

    @Override
    public Object[] toArray() {
        Node<T> temp = headElement;
        Object[] array = new Object[size];
        for (int i = 0; i < size; i++) {
            array[i] = temp.getElement();
            temp = temp.getNext();
        }

        return array;
    }

    @Override
    public ListIterator<T> listIterator() {
        return new MyListIterator<>(headElement);
    }

    @Override
    public Iterator<T> iterator() {
        throw new UnsupportedOperationException();
    }

    @Override
    public T get(int index) {
        throw new UnsupportedOperationException();
    }

    @Override
    public T set(int index, T element) {
        throw new UnsupportedOperationException();
    }

    @Override
    public void add(int index, T element) {
        throw new UnsupportedOperationException();
    }

    @Override
    public T remove(int index) {
        throw new UnsupportedOperationException();
    }

    @Override
    public int indexOf(Object o) {
        throw new UnsupportedOperationException();
    }

    @Override
    public int lastIndexOf(Object o) {
        throw new UnsupportedOperationException();
    }

    @Override
    public ListIterator<T> listIterator(int index) {
        throw new UnsupportedOperationException();
    }

    @Override
    public List<T> subList(int fromIndex, int toIndex) {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean remove(Object o) {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean addAll(int index, Collection<? extends T> c) {
        return false;
    }

    @Override
    public boolean contains(Object o) {
        throw new UnsupportedOperationException();
    }

    @Override
    public Object[] toArray(Object[] a) {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean add(T t) {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean addAll(Collection c) {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean retainAll(Collection c) {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean removeAll(Collection c) {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean containsAll(Collection c) {
        throw new UnsupportedOperationException();
    }

    class MyListIterator<T extends Flat> implements ListIterator<T> {
        private Node<T> node;
        private Node<T> last;

        MyListIterator(Node<T> node) {
            this.node = node;
        }

        @Override
        public boolean hasNext() {
            return node != null;
        }

        @Override
        public T next() {
            if (!hasNext()) {
                throw new NoSuchElementException();
            }

            last = node;
            node = node.getNext();

            return last.getElement();
        }

        @Override
        public void set(T flat) {
            last.setElement(flat);
        }

        @Override
        public void remove() { }

        @Override
        public void add(T t) { }

        @Override
        public boolean hasPrevious() {
            return false;
        }

        @Override
        public T previous() {
            return null;
        }

        @Override
        public int previousIndex() {
            return 0;
        }

        @Override
        public int nextIndex() {
            return 0;
        }
    }
}

