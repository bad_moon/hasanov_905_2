package ru.kpfu.itis.group905.hasanov.inf.stack;

public class Node<T> {
    private T element;
    private Node<T> next;

    // при импорте в последствии могут возникнуть проблемы из-за того, что конструктор не публичный
    // с другой стороны, если не подразумевается, что класс будет использвоаться вне пакета
    // (а в данном случае и вне стека его использование бессмысленное)
    // то возможно это должен быть вложенный класс

    // Причина по которой я сделал node не вложеным - его используют еще 3 класса stack и чтобы было
    // удобнее с ним работать я сделал его в отдельном классе, надеюсь это не критично
    public Node(T element) {
        this(element, null);
    }

    public Node(T element, Node<T> next) {
        this.element = element;
        this.next = next;
    }

    @Override
    public String toString() {
        return element.toString();
    }

    public T getElement() {
        return element;
    }

    public void setElement(T element) {
        this.element = element;
    }

    public Node<T> getNext() {
        return next;
    }

    public void setNext(Node<T> next) {
        this.next = next;
    }
}
