package ru.kpfu.itis.group905.hasanov.inf.stack.reformat;

import ru.kpfu.itis.group905.hasanov.inf.kr1.StackIsEmptyException;

public class StackOperation implements StackElement {
    protected StackOperation operation;
    protected int priority;

    public StackOperation() {}

    public StackOperation(String operation, Stack<StackElement> stack) {
        switch (operation) {
            case "+": {
                this.operation = new SumOperation(stack);
                priority = 1;
                break;
            }
            case "-": {
                this.operation = new DiffOperation(stack);
                priority = 1;
                break;
            }
            case "*": {
                this.operation = new MultOperation(stack);
                priority = 2;
                break;
            }
            case "/": {
                this.operation = new DivOperation(stack);
                priority = 2;
                break;
            }
            case "^": {
                this.operation = new PowOperation(stack);
                priority = 3;
                break;
            }
        }
    }

    public double value() throws StackIsEmptyException {
        double res = operation.value();
        return res;
    }

    public boolean isPushNeeded(Stack<StackOperation> stack) throws StackIsEmptyException {
        return !stack.isEmpty() && this.priority <= stack.peek().priority;
    }

    @Override
    public String toString() {
        return operation.toString();
    }
}

