package ru.kpfu.itis.group905.hasanov.inf.stack.reformat;

import ru.kpfu.itis.group905.hasanov.inf.kr1.StackIsEmptyException;

public class PowOperation extends StackOperation {
    private Stack<StackElement> stack;

    public PowOperation(Stack<StackElement> stack) {
        this.stack = stack;
    }

    @Override
    public String toString() {
        return "^";
    }

    @Override
    public double value() throws StackIsEmptyException {
        stack.pop();

        StackElement firstElem = this.stack.pop();
        this.stack.peek().value();
        stack.push(firstElem);
        firstElem.value();

        double temp = stack.pop().value();
        double res = Math.pow(stack.pop().value(), temp);

        stack.push(new StackNumber(res));
        return res;
    }
}