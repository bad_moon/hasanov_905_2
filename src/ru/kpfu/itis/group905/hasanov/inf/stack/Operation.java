package ru.kpfu.itis.group905.hasanov.inf.stack;

interface Operation<T extends Number> {
    int execute(T x, T y);
}