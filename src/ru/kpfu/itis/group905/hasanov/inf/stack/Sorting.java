package ru.kpfu.itis.group905.hasanov.inf.stack;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.Scanner;

public class Sorting {
    public static void main(String[] args) throws FileNotFoundException {
        Scanner scanner = new Scanner(new FileReader(
                "src\\ru\\kpfu\\itis\\group905\\hasanov\\inf\\stack\\in.txt"));
        StackFlat<Flat> stack = new StackFlat<>();
        String name;
        double weight, height;
        Flat flat;
        while (scanner.hasNext()) {
            name = scanner.next();
            height = scanner.nextDouble();
            weight = scanner.nextDouble();
            flat = new Flat(name, height, weight);
            stack.push(flat);
        }
        scanner.close();
        stack.sort(Flat::compareTo);
        System.out.println(stack);
    }
}
