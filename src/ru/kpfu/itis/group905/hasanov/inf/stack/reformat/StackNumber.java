package ru.kpfu.itis.group905.hasanov.inf.stack.reformat;

public class StackNumber implements StackElement {
    private double value;

    StackNumber(double value) {
        this.value = value;
    }

    public double value() {
        return this.value;
    }

    @Override
    public String toString() {
        return value + "";
    }
}

