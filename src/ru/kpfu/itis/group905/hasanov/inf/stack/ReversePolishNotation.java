package ru.kpfu.itis.group905.hasanov.inf.stack;

import ru.kpfu.itis.group905.hasanov.inf.kr1.StackIsEmptyException;

import java.util.Scanner;

// Здорово, что вы унифицировали многие вещи, еще немного доведем это до ума
// Туду: желтая подсветка это потенциальные места для упрощения или ошибок. Постарайтесь избегать их везде, где это возможно
// Туду: старайтесь называть методы так, чтобы
//      1) это отражало суть того, что происходит внутри
//      2) конечная композиция читалась человеко-понятно (stack.mathOperation(action(notation.charAt(i)));)

// Теперь Туду)
// Done: 1) Исправил все места с желтой подсветкой, кроме while (true) и не получилось исправить при кастовании к типу
//               в классе Stack
// Done: 2) Постарался назвать методы по вашим советам
public class ReversePolishNotation {
    public static void main(String[] args) throws MathException, StackIsEmptyException {
        Scanner scanner = new Scanner(System.in);

        String elem;
        StackOperators<Character> operators = new StackOperators<>();
        StringBuilder notation = new StringBuilder();

        int num;
        char operat;
        while (true) {
            elem = scanner.next();
            while (!elem.equals("stop")) {
                char temp = elem.charAt(0);
                if (temp >= '0' && temp <= '9') {
                    num = Integer.parseInt(elem);
                    notation.append(num);
                } else {
                    operat = temp;
                    if (operators.isEmpty()) {
                        operators.push(operat);
                    } else if (operat == ')') {
                        while (operators.peek() != '(') {
                            notation.append(operators.pop());
                        }
                        operators.pop();
                    } else if (operatorPriority(operat)
                            < operatorPriority(operators.peek()) && operators.peek() != '(') {
                        while (!operators.isEmpty() && operators.peek() != '(') {
                            notation.append(operators.pop());
                        }
                        operators.push(operat);
                    }
                    else if (operatorPriority(operat)
                            == operatorPriority(operators.peek())) {
                        notation.append(operators.pop());
                        operators.push(operat);
                    } else {
                        operators.push(operat);
                    }
                }
                elem = scanner.next();
            }
            while (!operators.isEmpty()) {
                notation.append(operators.pop());
            }
            System.out.println(calculateValue(notation.toString()));
            notation = new StringBuilder();
        }
    }

    public static int operatorPriority(char input) throws MathException {
        if (input == '+' || input == '-') {
            return 1;
        } else if (input == '*' || input == '/') {
            return 2;
        } else if (input == '(' || input == ')') {
            return 3;
        } else if (input == '^') {
            return 4;
        }
        throw new MathException();
    }

    public static int calculateValue(String notation) throws StackIsEmptyException {
        Stack<Integer> stack = new Stack<>();
        Operation<Integer> operation;
        for (int i = 0; i < notation.length(); i++) {
            operation = action(notation.charAt(i));
            if (operation != null) {
                stack.mathOperation(operation);
            } else {
                stack.push(Integer.parseInt(notation.charAt(i) + ""));
            }
        }
        return stack.pop();
    }

    private static Operation<Integer> action(char type){
        switch(type){
            case '+': return Integer::sum;
            case '-': return (x, y) -> x - y;
            case '*': return (x, y) -> x * y;
            case '/': return (x, y) -> x / y;
            case '^': return (x, y) -> {
                if (y == 0) {
                    return 1;
                }
                for (int i = 2; i <= y; i++) {
                    x *= x;
                }
                return x;
            };
            default: return null;
        }
    }
}
