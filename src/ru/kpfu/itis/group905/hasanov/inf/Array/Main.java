/*
 * @author Anvar Hasanov
 * 11-905
 * Task Array
 */

package ru.kpfu.itis.group905.hasanov.inf.array;

public class Main {
    public static void main(String[] args) {
        Array array = new Array();
        array.add(1);
        array.add(2);
        array.add(3);
        array.add(0, 99);
        array.add(4, 99);
        System.out.println("Contains - " + array.contains(3));
        array.remove(0);
        array.delete(99);
        System.out.println(array);
        System.out.println("Get index - " + array.getIndex());
    }
}
