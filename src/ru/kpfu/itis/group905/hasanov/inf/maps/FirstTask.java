/*
 * @author Anvar Hasanov
 * 11-905
 * Task #1
 */

package ru.kpfu.itis.group905.hasanov.inf.maps;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.HashMap;
import java.util.Scanner;

public class FirstTask {
    public static void main(String[] args) throws FileNotFoundException {
        String path = "src\\ru\\kpfu\\itis\\group905\\hasanov\\inf\\maps\\";
        Scanner scanner = new Scanner(new FileReader(path + "in.txt"));

        HashMap<Integer, Integer> hashMap = new HashMap<>();
        Integer value, num;

        while (scanner.hasNextInt()) {
            num = scanner.nextInt();
            value = hashMap.get(num);
            hashMap.put(num, (value == null ? 1 : (value + 1)));
        }

        for (HashMap.Entry ent : hashMap.entrySet()) {
            System.out.println(ent.getKey() + " : " + ent.getValue() + " раз");
        }
    }
}