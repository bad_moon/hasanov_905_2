/*
 * @author Anvar Hasanov
 * 11-905
 * Task #2
 */

package ru.kpfu.itis.group905.hasanov.inf.maps;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class SecondTask {
    public static void main(String[] args) throws FileNotFoundException {
        String path = "src\\ru\\kpfu\\itis\\group905\\hasanov\\inf\\maps\\";
        Scanner scanner = new Scanner(new FileReader(path + "in1.txt"));

        Map<String, Integer> hashMap = new HashMap<>();
        String str;
        Integer value;

        while (scanner.hasNext()) {
            str = scanner.next().toLowerCase();
            value = hashMap.get(str);
            hashMap.put(str, (value == null ? 1 : (value + 1)));
        }

        for (HashMap.Entry ent : hashMap.entrySet()) {
            System.out.println(ent.getKey() + " : " + ent.getValue() + " раз");
        }
    }
}
