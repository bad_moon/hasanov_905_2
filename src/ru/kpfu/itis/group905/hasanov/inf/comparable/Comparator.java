/*
 * @author Anvar Hasanov
 * 11-905
 * Task #2 & #4
 */

package ru.kpfu.itis.group905.hasanov.inf.comparable;

public class Comparator implements java.util.Comparator<Integer> {
    int task;

    public Comparator() {
        this(0);
    }

    public Comparator(int task) {
        this.task = task;
    }

    @Override
    public int compare(Integer a, Integer b) {
        if (this.task == 2) {
            while (a != 0 && b != 0) {
                a /= 10;
                b /= 10;
            }
            return a == b ? 0 : (a == 0 ? -1 : 1);
        } else if (this.task == 4) {
            int newA = 0;
            int newB = 0;
            while (a != 0) {
                newA = newA * 10 + a % 10;
                a /= 10;
            }
            while (b != 0) {
                newB = newB * 10 + b % 10;
                b /= 10;
            }
            a = newA;
            b = newB;

        }
        return Integer.compare(a, b);
    }
}
