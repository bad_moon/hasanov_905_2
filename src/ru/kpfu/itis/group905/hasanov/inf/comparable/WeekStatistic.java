/*
 * @author Anvar Hasanov
 * 11-905
 * Task #3
 */

package ru.kpfu.itis.group905.hasanov.inf.comparable;

public class WeekStatistic implements Comparable<WeekStatistic> {
    private String name;
    private final int DAYS_IN_A_WEEK = 7;
    private long[] statistic = new long[DAYS_IN_A_WEEK];

    WeekStatistic(String name) {
         this.name = name;
    }

    public void insert(long number, int index) {
        this.statistic[index] = number;
    }

    @Override
    public int compareTo(WeekStatistic webSite) {
        int a = this.name.charAt(0);
        int b = webSite.name.charAt(0);
        if (a != b) {
            return a - b;
        }
        return 0;
    }

    @Override
    public String toString() {
        return  this.name + ": " + getStatistic();
    }

    public int getStatistic() {
        int result = 0;
        for (long count : this.statistic) {
            result += count;
        }
        return result / DAYS_IN_A_WEEK;
    }

    public String getName() {
        return this.name;
    }
}
