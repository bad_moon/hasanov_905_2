/*
 * @author Anvar Hasanov
 * 11-905
 * Task #3
 */

package ru.kpfu.itis.group905.hasanov.inf.comparable;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

public class ThirdTask {
    static final String path = "src\\ru\\kpfu\\itis\\group905\\hasanov\\inf\\comparable\\";
    static final int DAYS_IN_A_WEEK = 7;

    public static void main(String[] args) throws FileNotFoundException {
        Scanner scanner = new Scanner(new FileReader(path + "in.txt"));

        ArrayList<WeekStatistic> statisticsList = new ArrayList<>();
        WeekStatistic siteStatistic;
        long count;

        while (scanner.hasNextLine()) {
            siteStatistic = new WeekStatistic(scanner.next());
            for (int i = 0; i < DAYS_IN_A_WEEK; i++ ) {
                count = scanner.nextLong();
                siteStatistic.insert(count, i);
            }
            statisticsList.add(siteStatistic);
        }
        scanner.close();

        printWrite("out.txt", statisticsList);

        Collections.sort(statisticsList);

        printWrite("out2.txt", statisticsList);
    }

    public static void printWrite(String fileName,
                                  ArrayList<WeekStatistic> list) throws FileNotFoundException {
        PrintWriter printWriter = new PrintWriter(path + fileName);

        for (WeekStatistic webSite : list) {
            printWriter.println(webSite);
        }
        printWriter.close();
    }
}
