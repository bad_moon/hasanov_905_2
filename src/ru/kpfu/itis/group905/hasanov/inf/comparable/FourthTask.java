/*
 * @author Anvar Hasanov
 * 11-905
 * Task #4
 */

package ru.kpfu.itis.group905.hasanov.inf.comparable;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

public class FourthTask {
    public static void main(String[] args) throws FileNotFoundException {
        ArrayList<Integer> arrayList = new ArrayList<>();
        String path = "src\\ru\\kpfu\\itis\\group905\\hasanov\\inf\\comparable\\";
        Scanner scanner = new Scanner((new File(path + "in.txt")));

        while (scanner.hasNextInt()) {
            arrayList.add(scanner.nextInt());
        }

        Comparator comparator = new Comparator(4);

        Collections.sort(arrayList, comparator);

        PrintWriter printWriter = new PrintWriter(path + "out.txt");
        for (int value : arrayList) {
            printWriter.print(value + " ");

        }
        printWriter.close();

    }
}
