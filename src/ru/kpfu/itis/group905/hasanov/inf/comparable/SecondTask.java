/*
 * @author Anvar Hasanov
 * 11-905
 * Task #2
 */

package ru.kpfu.itis.group905.hasanov.inf.comparable;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

public class SecondTask {
    public static void main(String[] args) throws FileNotFoundException {
        ArrayList<Integer> list = new ArrayList<>();
        String path = "src\\ru\\kpfu\\itis\\group905\\hasanov\\inf\\comparable\\";
        Scanner scanner = new Scanner(new FileReader(path + "in.txt"));
        while (scanner.hasNextInt()) {
            list.add(scanner.nextInt());
        }
        scanner.close();

        Comparator comparator = new Comparator(2);

        Collections.sort(list, comparator);

        PrintWriter printWriter = new PrintWriter(path + "out.txt");
        for (int value : list) {
            printWriter.print(value + " ");

        }
        printWriter.close();
    }
}
