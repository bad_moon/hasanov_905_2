/*
 * @author Anvar Hasanov
 * 11-905
 * Task #1
 */

package ru.kpfu.itis.group905.hasanov.inf.comparable;

import java.io.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Scanner;

public class FirstTask {
    public static void main(String[] args) throws IOException {
        ArrayList<String> list = new ArrayList<>();
        String path = "src\\ru\\kpfu\\itis\\group905\\hasanov\\inf\\comparable\\";
        Scanner scanner = new Scanner((new File(path + "in.txt")));
        int k = scanner.nextInt();
        String text;
        while (scanner.hasNext()) {
            text = scanner.next();
            if (text.length() >= k) {
                list.add(text);
            }
        }
        scanner.close();

        PrintWriter print = new PrintWriter(new File(path + "out.txt"));

        java.util.Comparator<String> comparator;

        for (int i = 0; i < k; i++) {
            int finalI = i;
            comparator = Comparator.comparing(str -> str.charAt(finalI));
            Collections.sort(list, comparator);
            print.println(list.toString());
        }
        print.close();
    }
}
