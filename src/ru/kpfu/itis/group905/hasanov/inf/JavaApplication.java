package ru.kpfu.itis.group905.hasanov.inf;

import jdk.nashorn.internal.ir.debug.JSONWriter;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;

public class JavaApplication {
    private static String[] fields = {"first_name", "last_name", "about"};
    public static void main(String[] args) throws IOException {
        System.out.println(vkUserInform("anvar090"));
    }

    public static String vkUserInform(String userID) throws IOException {
        URL url = new URL(generateApiRequest(userID));
        InputStream inputStream = url.openStream();
        ByteArrayOutputStream byteArrayStream = new ByteArrayOutputStream();

        int c = inputStream.read();
        while (c != -1) {
            byteArrayStream.write(c);
            c = inputStream.read();
        }

        String response = byteArrayStream.toString();

        return jsonParser(response);
    }

    private static String jsonParser(String jsonString) {
        StringBuilder result = new StringBuilder();
        for (String field : fields) {
            int index = jsonString.indexOf(field);
            if (index != -1) {
                int i = index + field.length() + 3;
                while (i < jsonString.length()) {
                    if (jsonString.charAt(i) == '\"'
                            || jsonString.charAt(i) == ',') {
                        break;
                    }
                    i++;
                }
                result.append(jsonString, index + field.length() + 3, i);
                result.append(" ");
            }
        }
        return result.toString();
    }

    private static String generateApiRequest(String userID) {
        String accessToken = "d8677463d8677463d867746374d81566b3dd867d867746386a22c22e8c9336c80288378";
        String addressApi = "https://api.vk.com/method/users.get?";
        double versionApi = 5.103;

        StringBuilder url = new StringBuilder();
        url.append(addressApi);
        url.append("access_token=").append(accessToken);
        url.append("&v=").append(versionApi);
        url.append("&user_ids=").append(userID);

        if (fields.length > 0) {
            url.append("&fields=").append(fields[0]);
            for (int i = 1; i < fields.length; i++) {
                url.append(",").append(fields[i]);
            }
        }

        return url.toString();

    }
}
