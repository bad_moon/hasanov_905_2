package ru.kpfu.itis.group905.hasanov.inf.generics;

public class Node<T> {
    public T element;
    public Node next;
    public Node previous;
}
