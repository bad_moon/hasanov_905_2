/*
 * @author Anvar Hasanov
 * 11-905
 * Task MyList
 */

package ru.kpfu.itis.group905.hasanov.inf.generics;

import ru.kpfu.itis.group905.hasanov.inf.myList.MyIndexOutOfBoundException;

public class LinkedList<T>{
    private Node head;
    private Node tail;
    private int size;
    private int count;

    LinkedList(int size) {
        this.size = size;
    }

    public boolean add(T x) {
        if (count < size) {
            Node newNode = new Node();
            newNode.element = x;
            if (this.head == null) {
                this.head = newNode;
            }
            else {
                this.tail.next = newNode;
                newNode.previous = this.tail;
            }
            this.tail = newNode;
            count++;
            return true;
        }
        return false;
    }

    public void add(int k, T x) throws MyIndexOutOfBoundException {
        if (k < 0 || k >= size) {
            throw new MyIndexOutOfBoundException("Сan not add");
        }
        else if (k > count) {
            return;
        }
        else if (k == 0) {
            add(x);
        }
        else {
            Node temp = head;
            for (int i = 1; i < k; i++) {
                temp = temp.next;
            }
            pasteAfterOfNodeNewElement(temp, x);
        }
    }

    private void pasteAfterOfNodeNewElement(Node node, T element) {
        Node newNode = new Node();
        newNode.element = element;
        newNode.previous = node;
        if (node.equals(this.tail)) {
            this.tail = newNode;
        }
        else {
            node.next.previous = newNode;
        }
        newNode.next = node.next;
        node.next = newNode;
        count++;
    }

    public void set(int k, T x) throws MyIndexOutOfBoundException {
        if (k < 0 || k >= size) {
            throw new MyIndexOutOfBoundException("Сan not set, index exception");
        }
        Node temp = head;
        while (k != 0) {
            temp = temp.next;
            k--;
        }
        temp.element = x;
    }

    public boolean contains(T x) {
        Node temp = this.head;
        for (int i = 0; i < count; i++) {
            if (temp.element.equals(x)) {
                return true;
            }
            temp = temp.next;
        }
        return false;
    }

    public int add(T[] a) {
        int addCount = 0;
        while (this.count < this.size) {
            add(a[addCount]);
            addCount++;
        }
        return addCount;
    }

    public int contains(T[] a) {
        int containsCount = 0;
        Node temp = this.head;
        for (int i = 0; i < count; i++) {
            for (int j = 0; j < a.length; j++) {
                if (temp.element.equals(a[j])) {
                    containsCount++;
                    break;
                }
            }
            temp = temp.next;
        }
        return containsCount;
    }

    public void removeHead() {
        if (this.head != null) {
            if (this.head.next != null) {
                this.head = this.head.next;
                this.head.previous = null;
            }
            else {
                this.head = null;
                this.tail = null;
            }
            size--;
        }
    }

    public void removeTail() {
        if (this.head != null) {
            if (this.tail.previous != null) {
                this.tail = this.tail.previous;
                this.tail.next = null;
            }
            else {
                this.head = null;
                this.tail = null;
            }
            size--;
        }
    }

    public String toString() {
        Node temp = head;
        String result = "";
        while (temp != null) {
            result += temp.element + " ";
            temp = temp.next;
        }
        return result;
    }

    public T getHead() {
        return (T) head.element;
    }

    public T getTail() {
        return (T) tail.element;
    }

    public int getSize() {
        return size;
    }

    public int getCount() {
        return count;
    }
}
