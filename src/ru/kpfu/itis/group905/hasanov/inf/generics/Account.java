/* @author Anvar Hasanov
* 11-905
* Task #Accountable
*/

package ru.kpfu.itis.group905.hasanov.inf.generics;

public class Account<T1, T2 extends Number> implements Accountable{
    T1 id;
    T2 sum;

    Account (T1 id, T2 sum) {
        this.id = id;
        this.sum = sum;
    }

    @Override
    @SuppressWarnings("unchecked")
    public void setSum(int sum) {
        this.sum = (T2) (Number) sum;
    }

    @SuppressWarnings("unchecked")
    public void addToSum(T2 addSum) {
        this.sum = (T2) ((Number) (this.sum.doubleValue() + addSum.doubleValue()));
    }

    @SuppressWarnings("unchecked")
    public void subtractFromSum(T2 subSum) {
        this.sum = (T2) ((Number) (this.sum.doubleValue() - subSum.doubleValue()));
    }

    @Override
    public String toString() {
        return "id: " + this.id + ", sum: " + this.sum;
    }

    @Override
    public T1 getId() {
        return this.id;
    }

    @Override
    public T2 getSum() {
        return this.sum;
    }
}

