package ru.kpfu.itis.group905.hasanov.inf.generics;

interface Accountable<T1, T2> {
    T1 getId();
    T2 getSum();
    void setSum(int sum);
}