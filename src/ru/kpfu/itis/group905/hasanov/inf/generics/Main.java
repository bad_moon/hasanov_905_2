package ru.kpfu.itis.group905.hasanov.inf.generics;

import ru.kpfu.itis.group905.hasanov.inf.myList.MyIndexOutOfBoundException;

public class Main {
    public static void main(String[] args) throws MyIndexOutOfBoundException {
        Account[] arrayAcc = new Account[10];
        arrayAcc[0] = new Account<>("12345", 100);
        arrayAcc[1] = new Account<>("12346", 150);
        arrayAcc[2] = new Account<>("12348", 130);
        arrayAcc[3] = new Account<>("12349", 70);

        Array<Account> array = new Array<>(arrayAcc, 4);
        array.add(new Account("12350", 200));
        array.add(2, new Account("12347", 666));

        System.out.println("Contains? - " + array.contains(
                new Account("12347", 666)));
        array.remove(2);
        array.delete(new Account("12345", 100));

        System.out.println("Index - " + array.getIndex());
        System.out.println(array);

//        System.out.println("--------------------------------");
//
//        LinkedList<String> list = new LinkedList<>(7);
//        list.add("12");
//        list.add(1, "21");
//        list.add("123");
//        list.add(3, "228");
//        list.add("54");
//        list.set(0, "99");
//        System.out.println(list);
//        System.out.println("Contains one element- " + list.contains("228"));
//        System.out.println("Added - " + list.add(new String[] {"1", "2", "3", "4", "5"}));
//        System.out.println(list);
//        System.out.println("Contains elements - " + list.contains(new String[] {"21", "54", "99", "6", "9"}));
//        System.out.println("Head - " + list.getHead());
//        System.out.println("Tail - " + list.getTail());
//        System.out.println("Count - " + list.getCount());
    }
}
