package ru.kpfu.itis.group905.hasanov.inf.generics;

public class Array<T extends Accountable> {

    private T[] a;
    private int index;

    public Array(T[] inputArray, int index) {
        a = inputArray;
        this.index = index;
    }

    // adding x to the end of array
    public void add(T x) {
        if (index < a.length) {
            a[index] = x;
            index += 1;
        }
    }

    // adding x on position k. Be sure that position k is applicable to actual size of array.
    public void add(int k, T x) {
        if (k < 0 || k > index || a.length == index) {
            throw new IndexOutOfBoundsException("Can not add to array");
        }
        for (int i = index; i > k; i--) {
            a[i] = a[i - 1];
        }
        a[k] = x;
        index++;
    }

    // clearing the array
    public void clear() {
        index = 0;
    }

    // check if x is in array
    public boolean contains(T x) {
        for (int i = 0; i < index; i++) {
            if (a[i].getId().equals(x.getId())
                    && a[i].getSum().equals(x.getSum())) {
                return true;
            }
        }
        return false;
    }

    // remove an element from position k
    public void remove(int k) {
        index--;
        for (int i = k; i < index; i++) {
            a[i] = a[i + 1];
        }
    }

    // remove first occurence of x in array
    public void delete(T x) {
        for (int i = 0; i < index; i++) {
            if (a[i].getId().equals(x.getId())
                    && a[i].getSum().equals(x.getSum())) {
                index--;
                for (; i < index; i++) {
                    a[i] = a[i + 1];
                }
                break;
            }
        }
    }

    // convert array to string
    public String toString() {
        String result = "";
        for (int i = 0; i < index; i++) {
            result += a[i] + "\n";
        }
        return result;
    }

    public int getIndex() {
        return index;
    }
}
