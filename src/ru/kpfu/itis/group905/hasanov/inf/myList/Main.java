/*
 * @author Anvar Hasanov
 * 11-905
 * Task MyList
 */

package ru.kpfu.itis.group905.hasanov.inf.myList;

public class Main {
    public static void main(String[] args) throws MyIndexOutOfBoundException {
        MyArrayList arrayList = new MyArrayList(5);
        arrayList.add(3);
        arrayList.add(new int[] {1, 2, 1, 1, 3});
        arrayList.add(3, 99);
        System.out.println(arrayList);
        System.out.println("Contains - " + arrayList.contains(3));
        System.out.println("Count - " + arrayList.getCount());
        System.out.println("Count elements - " + arrayList.contains(new int[] {1, 2}));

        System.out.println("----------");

        MyLinkedList newLinked = new MyLinkedList(7);
        newLinked.add(12);
        newLinked.add(1, 21);
        newLinked.add(123);
        newLinked.add(3, 228);
        newLinked.add(54);
        newLinked.set(0, 99);
        System.out.println(newLinked);
        System.out.println("Contains one element- " + newLinked.contains(228));
        System.out.println("Added - " + newLinked.add(new int[] {1, 2, 3, 4, 5}));
        System.out.println(newLinked);
        System.out.println("Contains elements - " + newLinked.contains(new int[] {21, 54, 99, 6, 9}));
        System.out.println("Head - " + newLinked.getHead());
        System.out.println("Tail - " + newLinked.getTail());
        System.out.println("Count - " + newLinked.getCount());
    }
}
