/*
 * @author Anvar Hasanov
 * 11-905
 * Task MyList
 */

package ru.kpfu.itis.group905.hasanov.inf.myList;

public class MyLinkedList implements MyList{
    private Node head;
    private Node tail;
    private int size;
    private int count;

    MyLinkedList(int size) {
        this.size = size;
    }

    @Override
    public boolean add(int x) {
        if (count < size) {
            Node newNode = new Node();
            newNode.element = x;
            if (this.head == null) {
                this.head = newNode;
            }
            else {
                this.tail.next = newNode;
                newNode.previous = this.tail;
            }
            this.tail = newNode;
            count++;
            return true;
        }
        return false;
    }

    @Override
    public void add(int k, int x) throws MyIndexOutOfBoundException {
        if (k < 0 || k >= size) {
            throw new MyIndexOutOfBoundException("Сan not add");
        }
        else if (k > count) {
            return;
        }
        else if (k == 0) {
            add(x);
        }
        else {
            Node temp = head;
            for (int i = 1; i < k; i++) {
                temp = temp.next;
            }
            pasteAfterOfNodeNewElement(temp, x);
        }
    }

    private void pasteAfterOfNodeNewElement(Node node, int element) {
        Node newNode = new Node();
        newNode.element = element;
        newNode.previous = node;
        if (node.equals(this.tail)) {
            this.tail = newNode;
        }
        else {
            node.next.previous = newNode;
        }
        newNode.next = node.next;
        node.next = newNode;
        count++;
    }


    @Override
    public void set(int k, int x) throws MyIndexOutOfBoundException {
        if (k < 0 || k >= size) {
            throw new MyIndexOutOfBoundException("Сan not set, index exception");
        }
        Node temp = head;
        while (k != 0) {
            temp = temp.next;
            k--;
        }
        temp.element = x;
    }

    @Override
    public boolean contains(int x) {
        Node temp = this.head;
        for (int i = 0; i < count; i++) {
            if (temp.element == x) {
                return true;
            }
            temp = temp.next;
        }
        return false;
    }

    @Override
    public int add(int[] a) {
        int addCount = 0;
        while (this.count < this.size) {
            add(a[addCount]);
            addCount++;
        }
        return addCount;
    }

    @Override
    public int contains(int[] a) {
        int containsCount = 0;
        Node temp = this.head;
        for (int i = 0; i < count; i++) {
            for (int j = 0; j < a.length; j++) {
                if (temp.element == a[j]) {
                    containsCount++;
                    break;
                }
            }
            temp = temp.next;
        }
        return containsCount;
    }

    public String toString() {
        Node temp = head;
        String result = "";
        while (temp != null) {
            result += temp.element + " ";
            temp = temp.next;
        }
        return result;
    }

    public int getHead() {
        return head.element;
    }

    public int getTail() {
        return tail.element;
    }

    public int getSize() {
        return size;
    }

    public int getCount() {
        return count;
    }
}
