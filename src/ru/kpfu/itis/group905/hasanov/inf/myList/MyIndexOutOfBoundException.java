/*
 * @author Anvar Hasanov
 * 11-905
 * Task MyList
 */

package ru.kpfu.itis.group905.hasanov.inf.myList;

public class MyIndexOutOfBoundException extends Exception {
    public MyIndexOutOfBoundException(String text) {
        super(text);
    }
}
