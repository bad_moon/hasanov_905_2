/*
 * @author Anvar Hasanov
 * 11-905
 * Task MyList
 */

package ru.kpfu.itis.group905.hasanov.inf.myList;

public class MyArrayList implements MyList {
    private int[] array;
    private int count;

    MyArrayList(int size) {
        array = new int[size];
    }

    @Override
    public boolean add(int x) {
        if (count == array.length) {
            return false;
        }
        array[count] = x;
        count++;
        return true;
    }

    @Override
    public void add(int k, int x) throws MyIndexOutOfBoundException {
        if (k < 0 || k >= array.length) {
            throw new MyIndexOutOfBoundException("Сan not add");
        }
        else if (k < count) {
            if (count != array.length) {
                array[count] = array[count - 1];
            }
            for (int i = count - 1; i > k; i--) {
                array[i] = array[i-1];
            }
            array[k] = x;
            count++;
        }
        else {
            array[count] = x;
            count++;
        }
    }

    @Override
    public void set(int k, int x) throws MyIndexOutOfBoundException {
        if (k < 0 || k >= array.length) {
            throw new MyIndexOutOfBoundException("Are you idiot?");
        }
        array[k] = x;
    }

    @Override
    public boolean contains(int x) {
        for (int value : array) {
            if (value == x) {
                return true;
            }
        }
        return false;
    }

    @Override
    public int add(int[] a) {
        int countAdd = 0;
        int temp = count;
        for (int i = temp; i < array.length; i++) {
            array[i] = a[i - temp];
            count++;
        }
        return countAdd;
    }

    @Override
    public int contains(int[] a) {
        int temp = 0;
        for (int value : a) {
            for (int i : array) {
                if (value == i) {
                    temp++;
                    break;
                }
            }
        }
        return temp;
    }

    @Override
    public String toString() {
        String result = "";
        for (int value : array) {
            result += value + " ";
        }
        return result;
    }

    public int getCount() {
        return this.count;
    }

}
