/*
 * @author Anvar Hasanov
 * 11-905
 * Task MyList
 */

package ru.kpfu.itis.group905.hasanov.inf.myList;

public class Node {
    public int element;
    public Node next;
    public Node previous;
}
