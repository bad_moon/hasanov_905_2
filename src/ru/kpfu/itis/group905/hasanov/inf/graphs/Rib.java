package ru.kpfu.itis.group905.hasanov.inf.graphs;

public class Rib implements Comparable<Rib> {
    private Integer firstVertex;
    private Integer secondVertex;

    Rib(int firstVertex, int secondVertex) {
        this.firstVertex = firstVertex;
        this.secondVertex = secondVertex;
    }

    public int getFirstVertex() {
        return firstVertex;
    }

    public int getSecondVertex() {
        return secondVertex;
    }

    @Override
    public String toString() {
        return "[" + firstVertex + ", " + secondVertex + "]";
    }

    @Override
    public int compareTo(Rib rib) {
        if (!this.firstVertex.equals(rib.firstVertex)) {
            return this.firstVertex.compareTo(rib.firstVertex);
        }
        return this.secondVertex.compareTo(rib.secondVertex);
    }
}
