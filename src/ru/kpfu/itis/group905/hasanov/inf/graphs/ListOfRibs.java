package ru.kpfu.itis.group905.hasanov.inf.graphs;

import com.sun.istack.internal.NotNull;

import java.util.ArrayList;
import java.util.Arrays;

public class ListOfRibs implements GraphRepresentation {
    private ArrayList<Rib> list = new ArrayList<>();
    int countOfRibs;
    int minNumOfVertices;

    ListOfRibs(@NotNull int[][] array) {
        int max = 0;
        int min = array[0][0];
        for (int[] ints : array) {
            list.add(new Rib(ints[0], ints[1]));
            if (ints[0] > max || ints[1] > max) {
                max = Math.max(ints[0], ints[1]);
            }
            if (ints[0] < min || ints[1] < min) {
                min = Math.min(ints[0], ints[1]);
            }
        }
        minNumOfVertices = min;
        countOfRibs = max - min + 1;
        list.sort(new Comparator());
    }


    @Override
    public int getCountOfVertices() {
        return countOfRibs;
    }

    @Override
    public int getCountOfRibs() {
        return list.size();
    }

    @Override
    public int[] representationPO() {
        int[] arrayFO = new int[1 + getCountOfRibs() * 2 + getCountOfVertices()];
        arrayFO[0] = getCountOfVertices();
        int vertex = minNumOfVertices;
        for (int i = 1; i < arrayFO.length; i++) {
            for (Rib rib : list) {
                if (rib.getFirstVertex() == vertex) {
                    arrayFO[i] = rib.getSecondVertex() - minNumOfVertices + 1;
                    i++;
                } else if (rib.getSecondVertex() == vertex) {
                    arrayFO[i] = rib.getFirstVertex() - minNumOfVertices + 1;
                    i++;
                }
            }
            vertex++;
        }
        return Arrays.copyOf(arrayFO, arrayFO.length);
    }

    @Override
    public String toString() {
        return list.toString() + "\n";
    }
}
