package ru.kpfu.itis.group905.hasanov.inf.graphs;

import com.sun.istack.internal.NotNull;

import java.util.Arrays;

public class IncidenceMatrix implements GraphRepresentation {
    private int[][] matrix;

    IncidenceMatrix(@NotNull int[][] matrix) {
        this.matrix = Arrays.copyOf(matrix, matrix.length);
    }

    public String toString() {
        StringBuilder result = new StringBuilder();
        for (int[] array : matrix) {
            for (int num : array) {
                result.append(num).append(" ");
            }
            result.append("\n");
        }
        return result.toString();
    }

    @Override
    public int getCountOfVertices() {
        return matrix.length;
    }

    @Override
    public int getCountOfRibs() {
        return matrix[0].length;
    }

    @Override
    public int[] representationPO() {
        int[] arrayFO = new int[1 + 2 * getCountOfRibs() + getCountOfVertices()];
        arrayFO[0] = getCountOfVertices();
        int index = 1;
        for (int i = 0; i < getCountOfVertices(); i++) {
            for (int j = 0; j < getCountOfRibs(); j++) {
                if (matrix[i][j] > 0) {
                    if (matrix[i][j] == 1) {
                        arrayFO[index] = searchVertices(i, j) + 1;
                    } else if (matrix[i][j] == 2) {
                        arrayFO[index] = i + 1;
                    }
                    index++;
                }
            }
            arrayFO[index] = 0;
            index++;
        }
        return Arrays.copyOf(arrayFO, arrayFO.length);
    }

    private int searchVertices(int i, int j) {
        int index = 0;
        while (index < matrix.length) {
            if (matrix[index][j] == 1 && i != index) {
                break;
            }
            index++;
        }
        return index;
    }
}
