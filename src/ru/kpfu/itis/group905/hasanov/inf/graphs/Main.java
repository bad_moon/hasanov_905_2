/*
 Указания:
 1) граф не должен содержать несвязных вершин
 2) каждой вершине приписывается номер,
  поэтому не может быть случая когда полсе вершины 1 идет вершина 3
 */

package ru.kpfu.itis.group905.hasanov.inf.graphs;

public class Main {
    public static void main(String[] args) {
        int[][] array1 = new int[][]
                      { {0, 1, 1, 0},
                        {1, 0, 1, 1},
                        {1, 1, 0, 1},
                        {0, 1, 1, 0} };
        int[][] array2 = new int[][]
                        { {1, 1, 0, 0, 0},
                          {0, 1, 0, 1, 1},
                          {1, 0, 1, 1, 0},
                          {0, 0, 1, 0, 1} };
        int[][] array3 = new int[][] {
                {2, 3},
                {1, 3},
                {2, 4},
                {3, 4},
                {1, 2}
        };
        int[][] array4 = new int[][] {
                {2, 3},
                {1, 3, 4},
                {1, 2, 4},
                {2, 3}
        };

        AdjacencyMatrix graph1 = new AdjacencyMatrix(array1);
        System.out.println(checkAdjMatrix(graph1));

        IncidenceMatrix graph2 = new IncidenceMatrix(array2);
        System.out.println(checkIncMatrix(graph2));

        ListOfRibs graph3 = new ListOfRibs(array3);
        System.out.println(checkListOfRibs(graph3));

        AdjacencyList graph4 = new AdjacencyList(array4);
        System.out.println(checkAdjList(graph4));
    }

    //Вы сказали обязательно надо 12 стат методов

    public static IncidenceMatrix fromAdjMatrixToIncMatrix(AdjacencyMatrix graph) {
        return GraphTransformator.transformToIncidenceMatrix(graph);
    }

    public static IncidenceMatrix fromListOfRibsToIncMatrix(ListOfRibs graph) {
        return GraphTransformator.transformToIncidenceMatrix(graph);
    }

    public static IncidenceMatrix fromAdjListToIncMatrix(AdjacencyList graph) {
        return GraphTransformator.transformToIncidenceMatrix(graph);
    }

    public static AdjacencyMatrix fromIncMatrixToAdjMatrix(IncidenceMatrix graph){
        return GraphTransformator.transformToAdjacencyMatrix(graph);
    }

    public static AdjacencyMatrix fromListOfRibsToAdjMatrix(ListOfRibs graph){
        return GraphTransformator.transformToAdjacencyMatrix(graph);
    }

    public static AdjacencyMatrix fromAdjMatrixToAdjMatrix(AdjacencyList graph){
        return GraphTransformator.transformToAdjacencyMatrix(graph);
    }

    public static ListOfRibs fromIncMatrixToListOfRibs(IncidenceMatrix graph) {
        return GraphTransformator.transformToListOfRibs(graph);
    }

    public static ListOfRibs fromAdjMatrixToListOfRibs(AdjacencyMatrix graph) {
        return GraphTransformator.transformToListOfRibs(graph);
    }

    public static ListOfRibs fromAdjListToListOfRibs(AdjacencyList graph) {
        return GraphTransformator.transformToListOfRibs(graph);
    }

    public static AdjacencyList fromIncMatrixToAdjList(IncidenceMatrix graph) {
        return GraphTransformator.transformToAdjacencyList(graph);
    }

    public static AdjacencyList fromAdjMatrixToAdjList(AdjacencyMatrix graph) {
        return GraphTransformator.transformToAdjacencyList(graph);
    }

    public static AdjacencyList fromListOfRibsToAdjList(ListOfRibs graph) {
        return GraphTransformator.transformToAdjacencyList(graph);
    }

    public static IncidenceMatrix checkIncMatrix(IncidenceMatrix graph) {
        AdjacencyMatrix graph1 = fromIncMatrixToAdjMatrix(graph);
        AdjacencyList graph2 = fromAdjMatrixToAdjList(graph1);
        ListOfRibs graph3 = fromAdjListToListOfRibs(graph2);
        return fromListOfRibsToIncMatrix(graph3);
    }

    public static AdjacencyMatrix checkAdjMatrix(AdjacencyMatrix graph) {
        IncidenceMatrix graph1 = fromAdjMatrixToIncMatrix(graph);
        AdjacencyList graph2 = fromIncMatrixToAdjList(graph1);
        ListOfRibs graph3 = fromAdjListToListOfRibs(graph2);
        return fromListOfRibsToAdjMatrix(graph3);
    }

    public static ListOfRibs checkListOfRibs(ListOfRibs graph) {
        IncidenceMatrix graph1 = fromListOfRibsToIncMatrix(graph);
        AdjacencyMatrix graph2 = fromIncMatrixToAdjMatrix(graph1);
        AdjacencyList graph3 = fromAdjMatrixToAdjList(graph2);
        return fromAdjListToListOfRibs(graph3);
    }

    public static AdjacencyList checkAdjList(AdjacencyList graph) {
        IncidenceMatrix graph1 = fromAdjListToIncMatrix(graph);
        AdjacencyMatrix graph2 = fromIncMatrixToAdjMatrix(graph1);
        ListOfRibs graph3 = fromAdjMatrixToListOfRibs(graph2);
        return fromListOfRibsToAdjList(graph3);
    }
}