package ru.kpfu.itis.group905.hasanov.inf.graphs;

import com.sun.istack.internal.NotNull;

import java.util.Arrays;

public class AdjacencyMatrix implements GraphRepresentation {
    private int[][] matrix;
    private int countOfRibs = -1;

    AdjacencyMatrix(@NotNull int[][] matrix) {
        this.matrix = Arrays.copyOf(matrix, matrix.length);
    }

    @Override
    public String toString() {
        StringBuilder result = new StringBuilder();
        for (int[] array : matrix) {
            for (int num : array) {
                result.append(num).append(" ");
            }
            result.append("\n");
        }
        return result.toString();
    }

    @Override
    public int getCountOfVertices() {
        return matrix.length;
    }

    @Override
    public int getCountOfRibs() {
        if (matrix != null && countOfRibs == -1) {
            countOfRibs = 0;
            for (int i = matrix.length - 1; i >= 0; i--) {
                for (int j = 0; j <= i; j++) {
                    countOfRibs += matrix[i][j];
                }
            }
            return countOfRibs;
        }
        return countOfRibs != -1 ? countOfRibs : 0;
    }

    @Override
    public int[] representationPO() {
        int[] arrayFO = new int[1 + 2 * getCountOfRibs() + getCountOfVertices()];
        arrayFO[0] = getCountOfVertices();
        int index = 1;
        for (int[] ints : matrix) {
            for (int j = 0; j < ints.length; j++) {
                if (ints[j] > 0) {
                    for (int i = 1; ints[j] - i >= 0; i++) {
                        arrayFO[index] = j + 1;
                        index++;
                    }
                }
            }
            index++;
        }
        return Arrays.copyOf(arrayFO, arrayFO.length);
    }
}
