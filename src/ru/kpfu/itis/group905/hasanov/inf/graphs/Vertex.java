package ru.kpfu.itis.group905.hasanov.inf.graphs;

public class Vertex {
    private int vertex;
    private Vertex next;

    Vertex(int vertex) {
        this(vertex, null);
    }

    Vertex(int vertex, Vertex next) {
        this.vertex = vertex;
        this.next = next;
    }

    public int getVertex() {
        return vertex;
    }

    public Vertex getNext() {
        return next;
    }

    public void setNext(Vertex next) {
        this.next = next;
    }

    @Override
    public String toString() {
        return vertex + "";
    }
}
