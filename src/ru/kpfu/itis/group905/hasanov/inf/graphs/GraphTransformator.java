package ru.kpfu.itis.group905.hasanov.inf.graphs;

import java.util.HashSet;

public class GraphTransformator {

    public static AdjacencyMatrix transformToAdjacencyMatrix(GraphRepresentation graph) {
        int[] array = graph.representationPO();
        int[][] matrix = new int[array[0]][array[0]];
        int indexVer = 0;
        for (int i = 1; i < array.length; i++) {
            if (array[i] == 0) {
                indexVer++;
                continue;
            }
            matrix[indexVer][array[i] - 1] += 1;
        }
        return new AdjacencyMatrix(matrix);
    }

    public static IncidenceMatrix transformToIncidenceMatrix(GraphRepresentation graph) {
        int[] array = graph.representationPO();
        int[][] matrix = new int[array[0]][(array.length - 1 - array[0]) / 2];
        int indexVer = 0;
        int indexRibs = 0;
        for (int i = 1; i < array.length && indexRibs < matrix[0].length; i++) {
            if (array[i] != 0) {
                if (array[i] - 1 >= indexVer) {
                    matrix[indexVer][indexRibs] += 1;
                    matrix[array[i] - 1][indexRibs] += 1;
                    indexRibs++;
                }
            } else {
                indexVer++;
            }
        }
        return new IncidenceMatrix(matrix);
    }

    public static ListOfRibs transformToListOfRibs(GraphRepresentation graph) {
        int[] array = graph.representationPO();
        HashSet<Integer> hashSet = new HashSet<>();
        int[][] listRibs = new int[graph.getCountOfRibs()][2];
        int vertexCount = 1;
        int index = 0;
        for (int i = 1; i < array.length; i++) {
            while (array[i] != 0) {
                if (!hashSet.contains(array[i])) {
                    listRibs[index][0] = vertexCount;
                    listRibs[index][1] = array[i];
                    index++;
                }
                i++;
            }
            hashSet.add(vertexCount);
            vertexCount++;
        }
        return new ListOfRibs(listRibs);
    }

    public static AdjacencyList transformToAdjacencyList(GraphRepresentation graph) {
        int[] array = graph.representationPO();
        int[][] adjList = new int[array[0]][];
        int index = 1;
        int count;
        for (int i = 0; i < adjList.length; i++) {
            count = 0;
            while(array[index] != 0) {
                count++;
                index++;
            }
            adjList[i] = new int[count];
            System.arraycopy(array, index - count, adjList[i], 0, count);
            index++;
        }
        return new AdjacencyList(adjList);
    }

}
