package ru.kpfu.itis.group905.hasanov.inf.graphs;

public interface GraphRepresentation {

    int getCountOfVertices();

    int getCountOfRibs();

    int[] representationPO();

}
