package ru.kpfu.itis.group905.hasanov.inf.graphs;

import com.sun.istack.internal.NotNull;

import java.util.ArrayList;

public class AdjacencyList implements GraphRepresentation {
    private ArrayList<Vertex> arrayList = new ArrayList<>();
    private int countOfVertices;
    private int countOfRibs = -1;

    AdjacencyList(@NotNull int[][] array) {
        arrayList.add(new Vertex(0));
        int vertexNum = 1;
        Vertex vertexHead, vertexTemp;
        for (int[] subarray : array) {
            if (subarray.length > 0) {
                vertexTemp = new Vertex(subarray[subarray.length - 1]);
                vertexHead = vertexTemp;
                for (int i = subarray.length - 2; i >= 0; i--) {
                    vertexHead = new Vertex(subarray[i], vertexTemp);
                    vertexTemp = vertexHead;
                }
                arrayList.add(vertexNum, vertexHead);
            }
            vertexNum++;
        }
        countOfVertices = arrayList.size() - 1;
    }

    @Override
    public int getCountOfVertices() {
        return countOfVertices;
    }

    @Override
    public int getCountOfRibs() {
        if (countOfRibs == -1) {
            countOfRibs += 1;
            Vertex temp;
            for (int i = 1; i < arrayList.size(); i++) {
                temp = arrayList.get(i);
                while (temp != null) {
                    if (i <= temp.getVertex()) {
                        countOfRibs++;
                    }
                    temp = temp.getNext();
                }
            }
        }
        return countOfRibs;
    }

    @Override
    public int[] representationPO() {
        int[] arrayFO = new int[1 + getCountOfRibs() * 2 + getCountOfVertices()];
        arrayFO[0] = getCountOfVertices();
        int index = 1;
        for (int i = 1; i < arrayList.size(); i++) {
            Vertex temp = arrayList.get(i);
            while (temp != null) {
                arrayFO[index] = temp.getVertex();
                index++;
                temp = temp.getNext();
            }
            index++;
        }
        return arrayFO;
    }

    @Override
    public String toString() {
        StringBuilder result = new StringBuilder();
        Vertex temp;
        for (int i = 1; i < arrayList.size(); i++) {
            result.append(i).append(": ");
            temp = arrayList.get(i);
            while (temp != null) {
                result.append(temp).append(" ");
                temp = temp.getNext();
            }
            result.append("\n");
        }
        return result.toString();
    }
}
