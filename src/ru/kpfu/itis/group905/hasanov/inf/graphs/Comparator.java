package ru.kpfu.itis.group905.hasanov.inf.graphs;

public class Comparator implements java.util.Comparator<Rib> {
    @Override
    public int compare(Rib rib1, Rib rib2) {
        return rib1.compareTo(rib2);
    }
}
