/*
 * @author Anvar Hasanov
 * 11-905
 * Task #3
 */

package ru.kpfu.itis.group905.hasanov.inf.regex;

import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Task3 {
    public static void main(String[] args) {
        Pattern pattern = Pattern.compile("[13579]{3}");
        Random random = new Random();
        Matcher matcher;
        int count = 0;
        int i = 0;
        while (count != 10) {
            String event = String.valueOf(random.nextInt(1000000000));
            matcher = pattern.matcher(event);
            if (!matcher.find()) {
                System.out.println(event);
                count++;
            }
            i++;
        }
        System.out.println("Iteration = " + i);
    }
}
