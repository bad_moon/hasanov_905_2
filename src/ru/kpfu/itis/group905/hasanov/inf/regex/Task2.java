/*
 * @author Anvar Hasanov
 * 11-905
 * Task #2
 */

package ru.kpfu.itis.group905.hasanov.inf.regex;

public class Task2 {
    public static String MY_NUM_FORMAT = "[-+]([1-9]\\d*)|0[,.](\\d*[1-9]|0*\\(\\d*\\))";
    public static void main(String[] args) {

        String number = "0,00(34)";

        System.out.println(numberValidation(number));
    }

    public static boolean numberValidation(String number) {
        return number.matches(MY_NUM_FORMAT);
    }

}
