/*
 * @author Anvar Hasanov
 * 11-905
 * Task #1
 */

package ru.kpfu.itis.group905.hasanov.inf.regex;

public class Task1 {
    public static String MY_DATE_FORMAT_1 = "(((0[1-9]|[12][0-9]|30)/(0[4-9]|1[012])|(0[789]|[12][0-9]|30)/03" +
            "|31/(0[3578]|1[02]))/1237) ([01][0-9]|2[0-3]):[0-5][0-9]";
    public static String MY_DATE_FORMAT_2 = "06/03/1237 (1[2-9]|2[0-3]):[0-5][0-9]";
    public static String MY_DATE_FORMAT_3 = "(((0[1-9]|[12][0-9]|30)/(0[13-9]|1[012])|31/(0[13578]|1[02])" +
            "|(0[1-9]|1[0-9]|2[0-8])/02)/1(([3-8]([0-9]{2}))|(2[4-9][0-9])|(23[89])|(9[0-6][0-9])|(97[0-7]))" +
            "|29/02/(1[3-8](([2468][048]|[02468][48])|[13579][26])|(1[3-9])00)) ([01][0-9]|2[0-3]):[0-5][0-9]";
    public static String MY_DATE_FORMAT_4 = "(((0[1-9]|[12][0-9]|3[01])/01|(0[1-9]|1[0-9]|2[0-6])/02)/1978) " +
            "([01][0-9]|2[0-3]):[0-5][0-9]";
    public static String MY_DATE_FORMAT_5 = "27/02/1978 (([01][0-9]|20):[0-5][0-9]|21:([0-2][0-9]|3[0-5]))";

    public static void main(String[] args) {
        String date = "27/02/1978 21:35";

        System.out.println(dateValidation(date));

    }

    public static boolean dateValidation(String date) {
        return date.matches(MY_DATE_FORMAT_1) || date.matches(MY_DATE_FORMAT_2)
                || date.matches(MY_DATE_FORMAT_3) || date.matches(MY_DATE_FORMAT_4)
                || date.matches(MY_DATE_FORMAT_5);
    }
}
