package ru.kpfu.itis.group905.hasanov.inf.files;

import java.io.File;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.Objects;

public class FileCounting {
    static ArrayList<File> fileList = new ArrayList<>();

    public static void main(String[] args) {
        String path = "src\\ru\\kpfu\\itis\\group905\\";
        File file = new File(path);

        findJavaFiles(file);

        fileList.sort(Comparator.comparing((File::lastModified)));

        System.out.println(filesInfo(fileList));
    }

    public static String filesInfo(ArrayList<File> fileArrayList) {
        StringBuilder result = new StringBuilder();
        result.append("Count of files - ").append(fileArrayList.size()).append("\n");
        for (File file : fileArrayList) {
            result.append(file.getName()).append("\t");
            result.append("Last modified - ").append(new Date(file.lastModified()));
            result.append("\n");
        }
        return result.toString();
    }

    public static void findJavaFiles(File file) {
        for (File elem : Objects.requireNonNull(file.listFiles())) {
            if (elem.isDirectory()) {
                findJavaFiles(elem);
            } else {
                if (elem.getName().endsWith(".java")) {
                    fileList.add(elem);
                }
            }
        }
    }
}
