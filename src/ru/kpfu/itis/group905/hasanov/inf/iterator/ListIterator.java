/*
 * @author Anvar Hasanov
 * 11-905
 * Task #2
 */

package ru.kpfu.itis.group905.hasanov.inf.iterator;

import ru.kpfu.itis.group905.hasanov.algorithms.pair1.Node;

public class ListIterator implements java.util.ListIterator<Integer> {
    private Node lastNode;
    private Node node;
    private int nextIndex;
    private int lastCount;
    private int size;

    ListIterator(Node node, int size) {
        this.node = node;
        this.size = size;
        this.nextIndex = 0;
    }

    @Override
    public boolean hasNext() {
        return nextIndex < size;
    }

    @Override
    public Integer next() {
        lastCount = node.elem;
        lastNode = node;

        int i = 2;
        do {
            node = node.next;
            nextIndex++;
            i--;
        } while (nextIndex + 1 < size && i > 0);

        return lastCount;
    }

    @Override
    public int nextIndex() {
        return this.nextIndex;
    }

    @Override
    public boolean hasPrevious() {
        return this.nextIndex > 0;
    }

    @Override
    public Integer previous() {
        if (nextIndex >= size) {
            node = lastNode.previous;
            nextIndex--;
        }

        lastCount = node.elem;
        lastNode = node;

        for (int i = 2; i > 0; i--) {
            node = node.previous;
            nextIndex--;
        }

        return lastCount;
    }

    @Override
    public int previousIndex() {
        return this.nextIndex - 1;
    }

    @Override
    public void remove() {}

    @Override
    public void set(Integer integer) {}

    @Override
    public void add(Integer integer) {}
}
