package ru.kpfu.itis.group905.hasanov.inf.iterator;

public class Main {
    public static void main(String[] args) {
        int[] array = new int[]{1, 2, 3, 4, 5, 6, 7, 8, 9};

        //First Task
        LinkedList list1 = new LinkedList(array);
        for (int temp : list1) {
            System.out.print(temp + " ");
        }
        System.out.println("\n");

        // Second Task
        LinkedListSecondTask list2 = new LinkedListSecondTask(array);
        ListIterator iterator = (ListIterator) list2.iterator();
        while (iterator.hasNext()) {
            System.out.print(iterator.next() + " ");
        }
        System.out.println();
        while (iterator.hasPrevious()) {
            System.out.print(iterator.previous() + " ");
        }
        System.out.println("\n");

        //Third Task
        java.util.LinkedList<Integer> list3 = new java.util.LinkedList<>();
        int i = 1;
        while (i < 11) {
            list3.add(i++);
        }
        System.out.println(list3);

        java.util.ListIterator<Integer> iterator2 =
                (java.util.ListIterator<Integer>) list3.iterator();

        for (int j = 0; j < 4; j++) {
            iterator2.next();
        }

        iterator2.remove();
        System.out.println(list3);

        //Может бросить ошибку, потому что предыдущий мы удалилли
        iterator2.next();
        iterator2.previous();
        iterator2.set(99);
        System.out.println(list3);

        //Добавляет перед элементом
        iterator2.add(666);
        System.out.println(list3);
    }
}
