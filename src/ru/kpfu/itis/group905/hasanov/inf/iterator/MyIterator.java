/*
 * @author Anvar Hasanov
 * 11-905
 * Task #1
 */

package ru.kpfu.itis.group905.hasanov.inf.iterator;

import ru.kpfu.itis.group905.hasanov.algorithms.pair1.Node;
import java.util.Iterator;

public class MyIterator implements Iterator<Integer> {
    private Node node;

    MyIterator(Node node) {
       this.node = node;
    }

    @Override
    public boolean hasNext() {
        return this.node != null;
    }

    @Override
    public Integer next() {
        int temp = this.node.elem;
        if (this.node.next != null) {
            this.node = this.node.next.next;
        } else {
            this.node = null;
        }
        return temp;
    }
}
