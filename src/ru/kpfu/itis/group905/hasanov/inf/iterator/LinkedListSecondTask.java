/*
* @author Anvar Hasanov
* 11-905
* Task #2
 */

package ru.kpfu.itis.group905.hasanov.inf.iterator;

import java.util.Iterator;

// Список иначально двухсвязный

public class LinkedListSecondTask extends LinkedList {

    LinkedListSecondTask(int[] array) {
        super(array);
    }

    @Override
    public Iterator<Integer> iterator() {
        return new ListIterator(getHead(), getSize());
    }
}
