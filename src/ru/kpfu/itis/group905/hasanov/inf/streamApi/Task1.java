package ru.kpfu.itis.group905.hasanov.inf.streamApi;

import java.io.*;
import java.util.*;
import java.util.stream.Collectors;

public class Task1 {
    static String path = "src\\ru\\kpfu\\itis\\group905\\hasanov\\inf\\streamApi\\";

    public static void main(String[] args) throws IOException {
        //first tack
        System.out.println(withoutStream());
        System.out.println(withStream());

        //second tack
        System.out.println(secondTask());
    }

    public static Map<String, ArrayList<String>> secondTask() throws IOException {
        HashMap<Integer, User> users = readUsersFromFile();
        InputStream inputStream = new FileInputStream(path + "subscriptions.csv");
        HashMap<String, ArrayList<String>> result = new HashMap<>();
        String data = new BufferedReader(new InputStreamReader(inputStream))
                .lines().collect(Collectors.joining("\n"));
        inputStream.close();

        int us, sub;
        for (String line : data.split("\n")) {
            String[] param = line.split(",");
            us = Integer.parseInt(param[0]);
            sub = Integer.parseInt(param[1]);

            if (users.get(us).getTown().equals(users.get(sub).getTown())) {
                User user = users.get(us);
                user.addSubscribe(sub);

                if (users.get(sub).checkSubscribe(us)) {
                    addFriend(result, users.get(sub).getName(), user.getName());
                }
            }
        }
        return result;
    }

    public static Map<String, ArrayList<String>> withStream() throws IOException {
        return addSubsFromFileAndRetFr(readUsersFromFile());
    }

    private static HashMap<String, ArrayList<String>> addSubsFromFileAndRetFr(
            HashMap<Integer, User> users) throws IOException {
        HashMap<String, ArrayList<String>> result = new HashMap<>();

        InputStream inputStream = new FileInputStream(path + "subscriptions.csv");
        String data = new BufferedReader(new InputStreamReader(inputStream))
                .lines().collect(Collectors.joining("\n"));
        inputStream.close();

        int us, sub;
        for (String line : data.split("\n")) {
            String[] param = line.split(",");
            us = Integer.parseInt(param[0]);
            sub = Integer.parseInt(param[1]);

            User user = users.get(us);
            user.addSubscribe(sub);

            if (users.get(sub).checkSubscribe(us)) {
                addFriend(result, users.get(sub).getName(), user.getName());
            }
        }

        return result;
    }

    private static HashMap<Integer, User> readUsersFromFile()
            throws IOException {
        InputStream inputStream = new FileInputStream(path + "users.csv");
        String data = new BufferedReader(new InputStreamReader(inputStream))
                .lines().collect(Collectors.joining("\n"));
        HashMap<Integer, User> users = new HashMap<>();
        inputStream.close();

        for (String line : data.split("\n")) {
            String[] param = line.split(",");
            User user = new User(Integer.parseInt(param[0]), param[1], param[2]);
            users.put(user.getId(), user);
        }

        return users;
    }

    public static Map<String, ArrayList<String>> withoutStream()
            throws FileNotFoundException {
        Scanner scanner = new Scanner(new FileReader(path + "users.csv"));
        HashMap<String, ArrayList<String>> result = new HashMap<>();
        HashMap<Integer, User> users = new HashMap<>();

        User user;
        String[] line;
        while (scanner.hasNext()) {
            line = scanner.nextLine().split(",");
            user = new User(Integer.parseInt(line[0]), line[1], line[2]);
            users.put(user.getId(), user);
        }

        scanner = new Scanner(new FileReader(path + "subscriptions.csv"));

        int us, sub;
        while (scanner.hasNext()) {
            line = scanner.nextLine().split(",");
            us = Integer.parseInt(line[0]);
            sub = Integer.parseInt(line[1]);

            user = users.get(us);
            user.addSubscribe(sub);

            if (users.get(sub).checkSubscribe(us)) {
                addFriend(result, users.get(sub).getName(), user.getName());
            }
        }

        return result;

    }

    private static void addFriend(HashMap<String, ArrayList<String>> users,
                                  String firstUserName, String secondUserName) {
        ArrayList<String> friends = users.getOrDefault(
                firstUserName, new ArrayList<>());
        friends.add(secondUserName);
        users.put(firstUserName, friends);

        friends = users.getOrDefault(secondUserName, new ArrayList<>());
        friends.add(firstUserName);
        users.put(secondUserName, friends);
    }
}
