package ru.kpfu.itis.group905.hasanov.inf.streamApi;

import java.util.ArrayList;

public class User {
    private int id;
    private String name;
    private String town;
    private ArrayList<Integer> subscribe = new ArrayList<>();

    public User(int id, String name, String town) {
        this.id = id;
        this.name = name;
        this.town = town;
    }

    public void addSubscribe(int userId) {
        subscribe.add(userId);
    }

    public boolean checkSubscribe(int userId) {
        return subscribe.contains(userId);
    }


//    public ArrayList<String> getFriendsNames() {
//        ArrayList<String> friends = new ArrayList<>();
//
//    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTown() {
        return town;
    }

    public void setTown(String town) {
        this.town = town;
    }

    public ArrayList<Integer> getSubscribe() {
        return subscribe;
    }

    public void setSubscribe(ArrayList<Integer> subscribe) {
        this.subscribe = subscribe;
    }
}
