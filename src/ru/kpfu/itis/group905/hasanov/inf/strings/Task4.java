/*
 * @author Anvar Hasanov
 * 11-905
 * Task #4
 */

package ru.kpfu.itis.group905.hasanov.inf.strings;

public class Task4 {
    public static void main(String[] args) {
        System.out.println(joinOdd("1 2 3 4 5 6 7 8 9"));
    }

    public static String joinOdd(String string) {
        StringBuilder result = new StringBuilder();
        string = string.trim();
        string = string.concat(" ");
        int lastCount = 0;
        int count = 0;
        count = string.indexOf(' ', count);
        while (count != -1 && lastCount != -1) {
            result.append(string, lastCount, count);
            lastCount = string.indexOf(' ', count + 1);
            count = string.indexOf(' ', lastCount + 1);
        }
        return result.toString();
    }
}
