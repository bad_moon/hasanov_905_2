/*
 * @author Anvar Hasanov
 * 11-905
 * Task #2
 */

package ru.kpfu.itis.group905.hasanov.inf.strings;

import com.sun.istack.internal.NotNull;

import java.util.HashMap;

public class Task2 {
    public static void main(String[] args) {
        System.out.println(count("PerfEcT wOrK"));
    }

    public static HashMap<Character, Integer> count(@NotNull String string) {
        HashMap<Character, Integer> hashMap = new HashMap<>();
        String[] words = string.toLowerCase().split(" ");
        int temp;
        for (String word : words) {
            for (int i = 0; i < word.length(); i++) {
                temp = hashMap.getOrDefault(word.charAt(i), 0);
                hashMap.put(word.charAt(i), temp + 1);
            }
        }
        return hashMap;
    }
}
