/*
 * @author Anvar Hasanov
 * 11-905
 * Task #1
 */

package ru.kpfu.itis.group905.hasanov.inf.strings;

import java.util.ArrayList;
import java.util.Arrays;

public class Task1 {
    public static void main(String[] args) {
        String[] strArray = new String[]{"g", "sdw", "sc", "a", "fa", "Aa"};

        ArrayList<String> list = new ArrayList<>(Arrays.asList(strArray));

        list.sort(String::compareTo);

        System.out.println(list);
    }
}
