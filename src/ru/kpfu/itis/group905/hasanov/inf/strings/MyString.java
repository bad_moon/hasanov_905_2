/*
 * @author Anvar Hasanov
 * 11-905
 * Task #1
 */

package ru.kpfu.itis.group905.hasanov.inf.strings;

import ru.kpfu.itis.group905.hasanov.inf.tree.MyTree;

public class MyString implements Comparable<String>{
    // Не получается унаследоваться от String т.к. он final
    private String value;

    public MyString() {}

    public MyString(String value) {
        this.value = value;
    }

    @Override
    public int compareTo(String anotherString) {
        return value.toLowerCase().compareTo(anotherString.toLowerCase());
    }
}
