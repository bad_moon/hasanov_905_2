/*
 * @author Anvar Hasanov
 * 11-905
 * Task #3
 */

package ru.kpfu.itis.group905.hasanov.inf.strings;

import com.sun.istack.internal.NotNull;

public class Task3 {
    public static void main(String[] args) {
        System.out.println("moma mom sadmomasd".replace("mom", "dad"));
        System.out.println(myReplace("moma mom sadmomasd", "mom", "dad"));
    }

    public static String myReplace(@NotNull String initial,
                                   @NotNull String oldWord, String newWord) {
        char[] chars = initial.toCharArray();
        StringBuilder result = new StringBuilder();
        for (int i = 0; i < chars.length; i++) {
            if (chars[i] == oldWord.charAt(0)) {
                int j = 0;
                boolean flag = true;
                while (i < chars.length && j < oldWord.length()) {
                    if (chars[i] != oldWord.charAt(j)) {
                        flag = false;
                        break;
                    }
                    i++;
                    j++;
                }

                if (flag && j == oldWord.length()) {
                    result.append(newWord);
                } else {
                    result.append(new String(chars, i - j, i));
                }
                i--;

            } else {
                result.append(chars[i]);
            }
        }
        return result.toString();
    }
}
